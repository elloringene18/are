@extends('master')

@inject('productService', 'App\Services\ProductProvider')
<?php $similar = $productService->getSimilar(15,$product['id']); ?>
<?php $favorites = $productService->getUserFavorites(); ?>

@section('css')
    <link href="{{ asset('public') }}/css/product.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
@endsection

@section('content')
    <style>
        .swiper-container {
            width: 100%;
            margin-left: auto;
            margin-right: auto;
        }

        .swiper-slide {
            background-size: cover;
            background-position: center;
        }

        .gallery-top {
            height: auto;
            margin-bottom: 30px;
        }

        /*.gallery-thumbs {*/
            /*height: 20%;*/
            /*box-sizing: border-box;*/
            /*padding: 10px 0;*/
        /*}*/

        .gallery-thumbs .swiper-slide {
            opacity: 0.4;
        }

        .gallery-thumbs .swiper-slide-thumb-active {
            opacity: 1;
        }

        .swiper-button-next.swiper-button-white, .swiper-button-prev.swiper-button-white {
            --swiper-navigation-color: #03930f;
        }

        .custom-select {
            margin-top: 0;
        }

        .label {
            font-size: 10px;
            font-weight: bold;
        }

        input[type=submit]:disabled {
            background-color: #ffffff;
            color: #ecd9bd;
            border: 1px solid #ecd9bd;
        }
        
        
    </style>

    <section id="content" class="inner-page">
        <div class="container">
            <div class="row">
                <div class="col-md-1"></div>
                <div class="col-md-11">
                    <div id="breadcrumbs">
                        <a href="{{ url('/') }}">Home</a> > <a href="{{ url('shop') }}">Shop</a> > {{$product->title}}
                    </div>
                </div>
            </div>
            <div class="row tc">
                <div class="col-md-1"></div>
                <div class="col-md-6">
                    <div class="swiper-container gallery-top">
                        <div class=""><img src="{{ $product->photoUrl }}" width="100%"></div>

                        @if($product->galleryPhotos)
                            @foreach($product->galleryPhotos as $photo)
                                <div class=""><img src="{{ $photo }}" width="100%"></div>
                            @endforeach
                        @endif
                    </div>
                    @if($product->galleryPhotos)
                        <div class="swiper-container gallery-thumbs pb-4">
                            <div class="swiper-slide"><img src="{{ $product->photoUrl }}" width="100%"></div>

                            @foreach($product->galleryPhotos as $photo)
                                <div class="swiper-slide"><img src="{{ $photo }}" width="100%"></div>
                            @endforeach
                        </div>
                    @endif
                </div>
                <div class="col-md-4 product">
                    <h1 class="brand mt-0">{{$product->title}}</h1>

                    <div class="details">
                        <span class="price">Details</span>
                        <p class="mt-3">{!! $product['description'] !!}</p>
                    </div>

                    {{--<h1 class="name">{{$product->category->name}}</h1>--}}
                    <form action="{{ url('cart/add') }}" method="post" id="addToCartForm">
                        <input type="hidden" value="{{$product->id}}" name="product_id">

                            @if($variants)
                                <div class="row">
                                    @foreach($variants as $key=>$variant)
                                        <div class="col-lg-12">
                                            <span class="label">SELECT {{ $key }}</span>
                                            <div class="custom-select mt-3" style="width:100%;">
                                                <select name="product_variant_id">
                                                    @foreach($variant as $value)
                                                        <option value="{{ $value['id'] }}">{{ $value['value'] }} (AED {{ $value['price'] }})</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            @else
                            @if($product->for_request==0)
                                <span class="price">{{$product->price}} AED</span> <span class="vat ml-3">Including VAT</span>
                            @else
                                <span class="price">Price per request</span>
                            @endif
                        @endif


                        @if($product->for_request==0)
                            <div class="row">
                                <div class="col-lg-12">
                                    <hr/>
                                    <span class="label">QUANTITIY</span>
                                    <div class="custom-select mt-3" style="width:100%;">
                                        <select name="qty">
                                            @for($x=1;$x<101;$x++)
                                                <option value="{{ $x }}">{{ $x }}</option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </div>
                        @endif


                        @if($product->for_request)
                            <a class="link-bt mt-4" href="mailto:info@a--re.com?subject=ARE PRODUCT REQUEST&body=I would like to order this item: {{ $product->title }}">REQUEST</a>
                        @elseif(Auth::user())
                            <input type="submit" class="link-bt mt-4" value="Add to Cart">
                        @else
                            <a href="#" class="poplink link-bt mt-4" id="" data-target="login-panel">Add to Cart</a>
                        @endif
                    </form>
                    <div class="details mt-2">
                        {{--<a href="#" class="txt-gold">Shipping and returns</a>--}}

                        <strong style="margin-top: 20px;">Delivery fee is not included.</strong>
                    </div>
                </div>
                <div class="col-md-1"></div>
            </div>
        </div>
    </section>
@endsection

@section('js')
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script>
    $('.gallery-top').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.gallery-thumbs',
        'prevArrow' : '<i class="slick-prev arrow left"></i>',
        'nextArrow' : '<i class="slick-next arrow right"></i>',
    });
    $('.gallery-thumbs').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.gallery-top',
        dots: false,
        spaceBetween: 10,
        focusOnSelect: true
    });
    //
    // var galleryThumbs = new Swiper('.gallery-thumbs', {
    //     spaceBetween: 10,
    //     slidesPerView: 4,
    //     freeMode: true,
    //     watchSlidesVisibility: true,
    //     watchSlidesProgress: true,
    //     breakpoints: {
    //         // when window width is >= 320px
    //         320: {
    //             slidesPerView: 2,
    //             spaceBetween: 20
    //         },
    //         // when window width is >= 480px
    //         480: {
    //             slidesPerView: 4,
    //             spaceBetween: 30
    //         },
    //     },
    // });
    //
    // var galleryTop = new Swiper('.gallery-top', {
    //     spaceBetween: 10,
    //     autoHeight: true,
    //     navigation: {
    //         nextEl: '.swiper-button-next',
    //         prevEl: '.swiper-button-prev',
    //     },
    //     thumbs: {
    //         swiper: galleryThumbs
    //     }
    // });

</script>
@endsection




