@extends('master')

@section('css')
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
<link rel="stylesheet" href="{{ asset('public/') }}/css/shop.css">
<style>
{{--#top {--}}
{{--background-image: url('{{ asset('public') }}/img/about-bg.png');--}}
{{--height: 100vh;--}}
{{--}--}}

#contents p {
margin-bottom: 4px;
}

h1 {
text-transform: uppercase;
}

#collections .slick-slide {
padding: 10px;
}

.slick-next {
right: -30px;
}

.slick-prev {
left: -30px;
}

.slick-arrow {
top: 46%;
}

@media only screen and (max-width: 620px){
#contents h1 {
font-size: 28px;
}
#collections {
padding: 0 60px;
}
.slick-next {
right: 20px;
}
.slick-prev {
left: 20px;
}

}

/*@media(min-width: 620px){*/
/*.prodwrap img{*/
/*    height:400px;*/
/*}*/
/*}*/

</style>
@endsection

@section('content')



@inject('productService', 'App\Services\ProductProvider')
<?php $products = $productService->getAll();
    $AllCat = $productService->getAllCat();
    ?>


@if($AllCat)
@foreach ($AllCat as $cat)

@if ($cat->name=='The Future of the past memory')
<div id="top">
    <div id="home-slide">
        <div><img src="{{ asset('public/uploads/products/1 (12).jpg') }}" class="" width="100%"></div>
    </div>
</div>
@endif

@if( $cat->name=='Reappropriating the Form')
<div id="top">
    <div id="home-slide">
        <div><img src="{{ asset('public/img/013.png') }}" class="" width="100%"></div>
    </div>
</div>
@endif

<section id="contents" class="container">
    <div class="clearfix">
        <div class="container" style="position: relative;">

            @if ($cat->name=='The Future of the past memory')
            
            <h1>{{ $cat->name }}</h1>

            <p>In times of change, reality becomes distorted and once familiar objects are recast to form new and alternative pieces.
                Traditional elements are reimagined from a new perspective to bridge the gap with the contemporary as an integral part of our identity.
                What remains constant is the connection with our physical surroundings; the sense of attachment to some places, and the remoteness of others. </p><br>

            <p>Season 01— the future of a past memory — explores living in a world where reality hinges on the surreal.
                explores living in a world where reality hinges on the surreal.
                This unexpected synthesis of contrasts— the alternating of color, size, shape, and position— nods toward a multitude of binaries.
                Here, opposites not only attract but work in concert,challenging the singular identity of everyday objects and spaces.</p><br>
            <p>The observer’s view is constructed by repositioning the point of convergence and altering the spatial perception.
                The contemporary interpretation of the traditional within this transformed reality ultimately shapes our sense of identity.
                This unique symbiosis between modernity and tradition is deeply rooted in the diversity that these pieces represent.</p>
            @endif





            @if( $cat->name=='Reappropriating the Form')
            <h1>{{ $cat->name }}</h1>
            <p>When tradition meets modernity, there are bound to be errors, glitches; but this is where the beauty of it all lies – in opposites. The sublime is ARE’s playground. </p>
            <p>ARE’s furniture is created through an artful combination of repetition, linear design, and complexity. Each piece captures a moment, freezing a digital frame, manipulating stability. Though a ‘glitch’ is temporary, here, it is transformed into a living art piece that lasts.</p>
            @endif


            <br />
            <div id="collections">
                @foreach($products as $product)
                @if ($product->category_id == $cat->id)
                <div class="">
                    <div class="prodwrap">
                        <img src="{{ $product->photoUrl }}" width="100%">
                    </div>
                </div>
                @endif
                @endforeach
            </div>

            <div class="addtocart">
                <a class="view-variant" href="{{ url('/collection-single/'.$cat->id) }}">SHOP THE COLLECTION</a>
            </div>
        </div>
    </div>
</section>
@endforeach
@endif



@endsection

@section('js')
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script>
    $(document).ready(function() {
        $('#collections , #collections2').slick({
            'autoplay': true
            , 'slidesToShow': 4
            , 'prevArrow': '<i class="slick-prev arrow left"></i>'
            , 'nextArrow': '<i class="slick-next arrow right"></i>'
            , responsive: [
                {
                breakpoint: 600
                , settings: {
                    slidesToShow: 1
                    , slidesToScroll: 1
                }
            },
            {
                breakpoint: 1000
                , settings: {
                    slidesToShow: 2
                    , slidesToScroll: 1
                }
            },
            {
                breakpoint: 1200
                , settings: {
                    slidesToShow: 3
                    , slidesToScroll: 1
                }
            },
            ]
        });

    });

</script>
@endsection
