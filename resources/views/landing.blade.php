


@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('public/') }}/css/masonry.css">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <style>
        #nav i {
            color: #fff;
        }

        #menu-bt {
            background-image: url('{{asset('public/img/menu-icon-white.png')}}');
        }

        #nav-logo {
            background-image: url('{{asset('public/img/are-logo-white.png')}}');
        }

        #nav .right a.green i {
            color: #03930F;
        }

        #nav .right a.green i {
            color: #03930F;
        }
        #menu-bt.green {
            background-image: url('{{asset('public/img/menu-icon.png')}}');
        }
        #nav-logo.green {
            background-image: url('{{asset('public/img/are-logo-black.png')}}');
        }
    </style>
@endsection

@section('content')
    @inject('productService', 'App\Services\ProductProvider')
    <?php $products = $productService->getSome(10,true); ?>
    <?php $products2 = $productService->getSome(10,true); ?>

    <div id="top">
        <div id="home-slide">
            <div><img src="{{ asset('public/img/home-bg3.jpg') }}" class="" width="100%"></div>
            <div><img src="{{ asset('public/img/home-bg2.jpg') }}" class="" width="100%"></div>
            <div><img src="{{ asset('public/img/home-bg.jpg') }}" class="" width="100%"></div>
        </div>
    </div>
    <section id="featured" class="clearfix">
        <div class="container" style="position: relative;">
            <h1>RESTORE. RECONSTRUCT. REINVENT. REDEFINE.</h1>
        </div>
        <div class="container-fluid" style="position: relative;">
            <div id="scroll-box">
                <div id="dbox">
                    <div class="step-left" id="parallax">
                        <div class="row">
                            <div class="col-md-12">
                                @foreach($products as $product)
                                    <img class="" src="{{ asset('public/'.$product->photo) }}" alt="{{$product->title}}" title="{{$product->title}}" width="200">
                                @endforeach
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                @foreach($products2 as $product)
                                    <img class="" src="{{ asset('public/'.$product->photo) }}" alt="{{$product->title}}" title="{{$product->title}}" width="200">
                                @endforeach
                            </div>
                        </div>
                        {{--<div class="brick">--}}
                        {{--<img src="{{ asset('public/') }}/images/oranges-pomegranates.jpg" alt="Oranges and Pomegranates" title="Oranges and Pomegranates">--}}
                        {{--</div>--}}
                        {{--<div class="brick">--}}
                        {{--<img src="{{ asset('public/') }}/images/strawberry.jpg" alt="Strawberry" title="Strawberry">--}}
                        {{--</div>--}}
                        {{--<div class="brick">--}}
                        {{--<img src="{{ asset('public/') }}/images/blueberries.jpg" alt="Blueberries" title="Blueberries">--}}
                        {{--</div>--}}
                        {{--<div class="brick">--}}
                        {{--<img src="{{ asset('public/') }}/images/pears.jpg" alt="Pears" title="Pears">--}}
                        {{--</div>--}}
                        {{--<div class="brick">--}}
                        {{--<img src="{{ asset('public/') }}/images/easter-eggs.jpg" alt="Easter-eggs" title="Easter-eggs">--}}
                        {{--</div>--}}
                        {{--<div class="brick">--}}
                        {{--<img src="{{ asset('public/') }}/images/lemons.jpg" alt="Lemons" title="Lemons">--}}
                        {{--</div>--}}
                        {{--<div class="brick">--}}
                        {{--<img src="{{ asset('public/') }}/images/cherries.jpg" alt="Cherries" title="Cherries">--}}
                        {{--</div>--}}
                        {{--<div class="brick">--}}
                        {{--<img src="{{ asset('public/') }}/images/grapes.jpg" alt="Grapes" title="Grapes">--}}
                        {{--</div>--}}
                        {{--<div class="brick">--}}
                        {{--<img src="{{ asset('public/') }}/images/strawberry.jpg" alt="Strawberry" title="Strawberry">--}}
                        {{--</div>--}}
                        {{--<div class="brick">--}}
                        {{--<img src="{{ asset('public/') }}/images/blueberries.jpg" alt="Blueberries" title="Blueberries">--}}
                        {{--</div>--}}
                        {{--<div class="brick">--}}
                        {{--<img src="{{ asset('public/') }}/images/pears.jpg" alt="Pears" title="Pears">--}}
                        {{--</div>--}}
                        {{--<div class="brick">--}}
                        {{--<img src="{{ asset('public/') }}/images/easter-eggs.jpg" alt="Easter-eggs" title="Easter-eggs">--}}
                        {{--</div>--}}
                        {{--<div class="brick">--}}
                        {{--<img src="{{ asset('public/') }}/images/lemons.jpg" alt="Lemons" title="Lemons">--}}
                        {{--</div>--}}
                        {{--<div class="brick">--}}
                        {{--<img src="{{ asset('public/') }}/images/cherries.jpg" alt="Cherries" title="Cherries">--}}
                        {{--</div>--}}
                    </div><!-- .masonry -->
                </div>
            </div>
        </div>
    </section>

    <section id="contents" class="container">
        <section id="featured-mobile" class="clearfix">
            <div class="container" style="position: relative;">
                <h1>RESTORE. RECONSTRUCT. REINVENT. REDEFINE.</h1>
                <div id="featured-slide">
                    @foreach($products as $product)
                        <div><img src="{{ asset('public/'.$product->photo) }}" alt="{{$product->title}}" title="{{$product->title}}" class="" width="100%"></div>
                    @endforeach
                </div>
            </div>
        </section>

        <section id="blog" class="clearfix">
            <div class="container" style="position: relative;">
                <div class="row">
                    <div class="col-md-3 blog-item">
                        <img src="{{ asset('public/') }}/img/blog-img.png" width="100%">
                        <h3 class="title">Lorem ipsum dolor sit amet, consectetuer adipiscing elit Lorem ipsum dolor sit</h3>
                        <p class="excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam. <a href="#">Continue Reading</a></p>
                    </div>
                    <div class="col-md-3 blog-item">
                        <img src="{{ asset('public/') }}/img/blog-img.png" width="100%">
                        <h3 class="title">Lorem ipsum dolor sit amet, consectetuer adipiscing elit Lorem ipsum dolor sit</h3>
                        <p class="excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam. <a href="#">Continue Reading</a></p>
                    </div>
                    <div class="col-md-3 blog-item">
                        <img src="{{ asset('public/') }}/img/blog-img.png" width="100%">
                        <h3 class="title">Lorem ipsum dolor sit amet, consectetuer adipiscing elit Lorem ipsum dolor sit</h3>
                        <p class="excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam. <a href="#">Continue Reading</a></p>
                    </div>
                    <div class="col-md-3 blog-item">
                        <img src="{{ asset('public/') }}/img/blog-img.png" width="100%">
                        <h3 class="title">Lorem ipsum dolor sit amet, consectetuer adipiscing elit Lorem ipsum dolor sit</h3>
                        <p class="excerpt">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam. <a href="#">Continue Reading</a></p>

                    </div>
                </div>
            </div>
        </section>
    </section>
@endsection

@section('js')
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/simple-parallax-js@5.5.1/dist/simpleParallax.min.js"></script>

    <script>
        $(window).on('load',function(){
            setTimeout(function(){
//               masonry();
            },300);
        });

        function parallax()
        {
            var image= document.getElementById('parallax');
            image.style.left= -(window.pageYOffset/ 5) + 'px';

        }
        window.addEventListener("scroll",parallax,false)


        // $(window).on('scroll',function(){ scrollF(); });
        //
        // function scrollF(){
        //     if($(window).scrollTop() > $('#featured').offset().top && $(window).scrollTop() <  ($('#blog').offset().top - $(window).height() ) && !$('#featured .follow').hasClass('active'))
        //         $('#featured .follow').addClass('active').css('left',$('#featured .container').offset().left);
        //     else if($(window).scrollTop() < $('#featured').offset().top )
        //         $('#featured .follow').removeClass('active');
        //     else if($(window).scrollTop() > ($('#blog').offset().top - $(window).height() ))
        //         $('#featured .follow').removeClass('active');
        //
        //     if($(window).scrollTop() > $('#scroll-box').offset().top){
        //         //        console.log($(window).scrollTop() - $('#scroll-box').offset().top);
        //         lft = $(window).scrollTop() - $('#scroll-box').offset().top;
        //         $('#scroll-box').css('left', $('#scroll-box').offset().top - $(window).scrollTop());
        //         $('#dbox').css('position', 'fixed');
        //     } else {
        //         $('#dbox').css('position', 'relative');
        //     }
        // }
        // scrollF();

        $(document).ready(function(){
            $('#featured-slide').slick({
                'autoplay' : true,
                'prevArrow' : '<i class="slick-prev arrow left"></i>',
                'nextArrow' : '<i class="slick-next arrow right"></i>',
            });
            $('#home-slide').slick({
                'autoplay' : true,
                'arrows' : false,
                'fade' : true
            });
        });

        $(window).on('scroll',function(){
            scrollCheck();
        });

        function scrollCheck(){
            scrollTop = $(this).scrollTop();
            if(scrollTop >= $('#top').height()){
                $('#nav-logo').addClass('green');
                $('#nav .right a').addClass('green');
            }
            else {
                $('#nav-logo').removeClass('green');
                $('#nav .right a').removeClass('green');
            }
        }
        scrollCheck();
    </script>
@endsection
