@extends('master')

@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <style>
        {{--#top {--}}
            {{--background-image: url('{{ asset('public') }}/img/about-bg.png');--}}
            {{--height: 100vh;--}}
        {{--}--}}

        #contents p {
            margin-bottom: 4px;
        }

    </style>
@endsection

@section('content')
    <div id="top">
        <div id="home-slide">
            <div><img src="{{ asset('public/img/about-bg2.jpg') }}" class="" width="100%"></div>
        </div>
    </div>
    <section id="contents" class="container">
        <div class="clearfix">
            <div class="container" style="position: relative;">
                <h1>ABOUT ARE</h1>
                <p>Memories and experiences of times past, intertwine to create pieces that reflect people, their souls and purpose. </p>
                <p>ARE fuses tradition and modernity, with a twist, a unique reminder of our collective yesterdays and a promise of brighter tomorrows. </p>
                <p>Amongst a playful collision of materials, patterns and colors, of repetition and exploration – between the traditional and the contemporary.</p>
                <p> ARE is a lucid dream captured, carefully taken apart and intricately reassembled to transform space. ARE transports you to where you belong. </p>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script>

        $(document).ready(function(){
            $('#home-slide').slick({
                'autoplay' : true,
                'arrows' : false,
                'fade' : true
            });
        });
    </script>
@endsection
