<section id="newsletter" class="clearfix container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-6 left">
                <span class="title">Subscribe to our newsletter</span>
                <p>Sign up for our newsletter and get the latest news and updates on collections, offers and more.</p>
            </div>
            <div class="col-md-6 right">
                <form id="subscribeForm" action="{{ url('/subscribe') }}" method="post">
                    @csrf
                    <div class="alert alert-success" id="subscribeSuccess"></div>
                    <div class="alert alert-success" id="subscribeError"></div>
                    <input type="text" name="email" placeholder="Enter your email">
                    <input type="submit" value="Go">
                </form>
            </div>
        </div>
    </div>
</section>

<section id="footer" class="clearfix container-fluid">
    <div class="container clearfix">
        <div class="row">
            <div class="left col-md-10 col-sm-10 col-xs-12">
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <ul>
                            <li class="heading"><a href="{{url('about-us')}}">About ARE</a></li>
                            <li class="heading"><a href="{{url('collections')}}">Collections</a></li>
                            <li class="heading"><a href="{{url('shop')}}">Products</a></li>
                            <li class="heading"><a href="{{url('contact-us')}}">Contact Us</a></li>
                            <li class="heading">&nbsp;</li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-12">
                        <p>info@a--re.com</p>
                        <ul class="socials">
                            <li><a target="_blank" href="https://www.instagram.com/are______________"><i class="fab fa-instagram"></i></a></li>
                            <li><a target="_blank" href="https://wa.me/971585564333"><i class="fab fa-whatsapp"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="right col-md-2 col-sm-2 col-xs-12">
                <a href="{{ url('/') }}" id="footer-logo"></a>
            </div>
        </div>
    </div>
</section>

<div id="message-bubble"></div>

<script src="{{ asset('public/') }}/js/vendor/jquery-3.6.0.min.js"></script>
<script src="{{ asset('public/') }}/js/vendor/modernizr-3.11.2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/simple-parallax-js@5.2.0/dist/simpleParallax.min.js"></script>
<script type="text/javascript" src="{{ asset('public/') }}/js/vendor/slick.min.js"></script>
<script type="text/javascript" src="{{ asset('public/') }}/js/vendor/images-loaded.js"></script>
<script type="text/javascript" src="{{ asset('public/') }}/js/vendor/masonry.js"></script>
<script type="text/javascript" src="{{ asset('public/') }}/js/vendor/bootstrap.min.js"></script>
<script src="{{ asset('public/') }}/js/plugins.js"></script>
<script src="{{ asset('public/') }}/js/main.js"></script>
<script src="{{ asset('public/') }}/js/login.js"></script>

<script>

    var image = document.getElementsByClassName('step-down');

    new simpleParallax(image, {
        overflow: true,
        orientation: 'down'
    });

    var image = document.getElementsByClassName('step-up');

    new simpleParallax(image, {
        overflow: true,
        orientation: 'up'
    });

    var image = document.getElementsByClassName('step-left');

    new simpleParallax(image, {
        overflow: true,
        orientation: 'left'
    });

    var image = document.getElementsByClassName('step-right');

    new simpleParallax(image, {
        overflow: true,
        orientation: 'right'
    });

    var image = document.getElementsByClassName('step-right-full');

    new simpleParallax(image, {
        overflow: true,
        orientation: 'right',
        maxTransition: 99
    });
    // external js: masonry.pkgd.js, imagesloaded.pkgd.js

    var grid = document.querySelector('.grid');
    var msnry;

    $(document).ready(function(){

    });

    function masonry(){
        // init Isotope after all images have loaded
        msnry = new Masonry( grid, {
            itemSelector: '.grid-item',
        });
    }


</script>

<script>

    $('.poplink').on('click',function (e) {
        e.preventDefault();
        $('.overlay').removeClass('active');
        $('#'+$(this).attr('data-target')).toggleClass('active');
    });

    var baseUrl = '{{ url('/') }}';

    function getFavourites(){
        $.ajax({
            type: "GET",
            url: baseUrl+'/favourites/get-favourites/',
            success: function(response){
                $('#nav a.wishlist span').text(response);

                if(response > 0) {
                    $('#nav a.wishlist span').addClass('active');
                } else {
                    $('#nav a.wishlist span').removeClass('active');
                }

            },
            statusCode: {
                401: function() {
                    window.location.href = baseUrl+'/login'; //or what ever is your login URI
                }
            },
            complete : function (event,error){
            }
        });
    }

    function updateCart(){
        $.ajax({
            type: "GET",
            url: baseUrl+'/cart/get-count/',
            success: function(response){
                $('#nav a.cart span').text(response);

                if(response > 0) {
                    $('#nav a.cart span').addClass('active');
                } else {
                    $('#nav a.cart span').removeClass('active');
                }

            },
            statusCode: {
                401: function() {
                    window.location.href = baseUrl+'/login'; //or what ever is your login URI
                }
            },
            complete : function (event,error){
            }
        });
    }

    @if(Auth::user())
    getFavourites();
    updateCart();
    @endif

    $('#loginForm').on('submit',function (e) {
        e.preventDefault();
        e.stopPropagation();

        var form = $(this);

        e.preventDefault();
        e.stopPropagation();

        data = form.serialize();
        url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(response){

                if(response.success) {
                    form.find('.form-error').hide();
                    location.reload();
                } else {
                    form.find('.form-error').show().empty().append(response.message);
                }

            },
            statusCode: {
                401: function() {
                    window.location.href = '{{url('login')}}'; //or what ever is your login URI
                }
            },
            complete : function (event,error){
            }
        });
    });

    $('#registerForm').on('submit',function (e) {
        e.preventDefault();
        e.stopPropagation();

        var form = $(this);

        e.preventDefault();
        e.stopPropagation();

        data = form.serialize();
        url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(response){

                if(response.success) {
                    form.find('.form-error').hide();
                    location.reload();
                    // $('#registerSuccess').show().empty().append(response.message);
                } else {
                    form.find('.form-error').show().empty().append(response.message);
                }

            },
            statusCode: {
                401: function() {
                    window.location.href = '{{url('login')}}'; //or what ever is your login URI
                }
            },
            complete : function (event,error){
            }
        });
    });

    $('.add-to-cart').on('click',function (e) {
        e.preventDefault();
        bt = $(this);
        id = bt.attr('data-id');
        bt.text('ADDING...');

        if(id){
            $.ajax({
                type: "GET",
                url: baseUrl+'/cart/add/'+id,
                success: function(response){

                    if(response.success) {
                        $('#message-bubble').show().empty().append(response.message);
                        bt.text('ADDED TO CART')
                    } else {
                        form.find('.form-error').show().empty().append(response.message);
                    }

                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                    updateCart();
                }
            });
        }
    });

    $('.wish.clickable').on('click',function (e) {
        e.preventDefault();
        e.stopPropagation();

        id = $(this).attr('data-id');
        elem = $(this);
        $.ajax({
            type: "GET",
            url: baseUrl+'/favourites/toggle/'+id,
            success: function(response){
                if(response>0) {
                    elem.addClass('active');
                } else {
                    elem.removeClass('active');
                }
            },
            statusCode: {
            },
            complete : function (event,error){
                getFavourites();
            }
        });
    });

    $('#addToCartForm').on('submit',function (e) {
        e.preventDefault();
        e.stopPropagation();

        var form = $(this);

        bt = $(this).find('input[type=submit]').first();
        console.log(bt);
        bt.attr('value','ADDING...');

        data = form.serialize();
        url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(response){

                if(response.success) {
                    $('#message-bubble').show().empty().append(response.message);
                    bt.attr('value','ADDED TO CART').attr('disabled','disabled');
                } else {
                    form.find('.form-error').show().empty().append(response.message);
                }

            },
            statusCode: {
                401: function() {
                    window.location.href = '{{url('login')}}'; //or what ever is your login URI
                }
            },
            complete : function (event,error){
                updateCart();
            }
        });
    });


    $('#subscribeForm').on('submit',function (e) {
        e.preventDefault();
        e.stopPropagation();

        data = $(this).serialize();
        url = $(this).attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(response){
                $('#subscribeSuccess').html(response).show();
                $('#subscribeForm').trigger('reset');
            }
        });

    });

</script>

@yield('js')

</body>

</html>
