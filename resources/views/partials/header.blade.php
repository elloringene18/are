<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title>ARE Studio</title>
    <meta name="description" content="ARE fuses tradition and modernity, with a twist, a unique reminder of our collective yesterdays and a promise of brighter tomorrows. Amongst a playful collision of materials, patterns and colors, of repetition and exploration – between the traditional and the contemporary.">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:title" content="ARE Studio">
    <meta property="og:type" content="">
    <meta property="og:url" content="">
    <meta property="og:image" content="">

    <link rel="manifest" href="{{ asset('public/') }}/site.webmanifest">
    <link rel="apple-touch-icon" href="{{ asset('public/') }}/icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="{{ asset('public/') }}/fonts/web/stylesheet.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/') }}/css/slick.min.css"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/') }}/css/bootstrap.min.css"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('public/') }}/css/normalize.css">
    <link rel="stylesheet" href="{{ asset('public/') }}/css/main.css?v=1.6">
    <link rel="stylesheet" href="{{ asset('public/') }}/css/login.css?v=1.1">
    <link href="{{ asset('public/') }}/fontawesome/css/all.css" rel="stylesheet">

    <meta name="theme-color" content="#fafafa">
    <style>
        body {
            font-family: 'Quicksand', sans-serif;
        }
    </style>

    @yield('css')
</head>

<body>
@include('partials.login')
@include('partials.register')

<nav id="nav-wrap" class="">
    <ul>
        <li style=""><a href="{{ url('/') }}">HOME</a></li>
        <li style=""><a href="{{ url('/about-us') }}">ABOUT ARE</a></li>
        <li style=""><a href="{{ url('/collections') }}">COLLECTIONS</a></li>
        <li style=""><a href="{{ url('/shop') }}">PRODUCTS</a></li>
        <li style=""><a href="{{ url('/news') }}">PRESS RELEASES</a></li>
        <li style=""><a href="{{ url('/exhibitions') }}">EXHIBITIONS</a></li>
        <li style=""><a href="{{ url('/contact-us') }}">CONTACT US</a></li>
    </ul>
</nav>

<header id="nav">
    <div class="row">
        <div class="col-4 text-start">
            <a href="{{ url('/') }}" id="nav-logo"></a>
        </div>
        <div class="col-8 text-end right">
            <a href="#" id="menu-bt"></a>

            @if(Auth::user())
                <a href="{{ url('/account') }}"><i class="fas fa-user"></i></a>
                <a href="{{ url('/favourites') }}" class="wishlist"><i class="fas fa-heart"></i><span class="{{ count(Auth::user()->favourites) ? 'active' : '' }}">{{ count(Auth::user()->favourites) > 0 ? count(Auth::user()->favourites) : '' }}</span></a>
                <a href="{{ url('/cart') }}" class="cart"><i class="fas fa-shopping-cart"></i><span class="{{count(Auth::user()->cart) ? 'active' : ''}}">{{ count(Auth::user()->cart) > 0 ? count(Auth::user()->cart) : '' }}</span></a>
            @else
                <a href="#" class="account poplink" id="" data-target="login-panel"><i class="fas fa-user"></i></a>
                <a href="#" class="cart poplink" id="cart-bt" data-target="login-panel"><i class="fas fa-shopping-cart"></i></a>
                <a href="#" class="favourites poplink" data-target="login-panel"><i class="fas fa-heart"></i></a>
            @endif

        </div>
    </div>
</header>


