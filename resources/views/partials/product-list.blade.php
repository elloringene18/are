@if(count($products))
    @foreach($products as $product)
            <div class="col-lg-4 col-md-6 product-col">
                <div class="prodwrap">
                    @if(Auth::user())
                        <span class="wish clickable {{ Auth::user()->favourites()->where('product_id',$product['id'])->count() ? 'active' : '' }}" data-id="{{$product->id}}"></span>
                    @else
                        <span class="wish poplink" id="" data-target="login-panel"></span>
                    @endif
                    <a href="{{ url('/product/'.$product['slug']) }}">
                        <img src="{{ $product->photoUrl }}" width="100%" >
                    </a>

                    @if(Auth::user())
                        <span class="wish clickable {{ Auth::user()->favourites()->where('product_id',$product['id'])->count() ? 'active' : '' }}" data-id="{{$product->id}}"><i class="fa fa-heart"></i></span>
                    @else
                        <span class="wish poplink" id="" data-target="login-panel"><i class="fa fa-heart"></i></span>
                    @endif
                </div>
                @if($product->is_faved)
                    <span class="popular"></span>
                @endif

                <div class="row">
                    <div class="col-md-10">
                        <a href="{{ url('/product/'.$product['slug']) }}">
                            <h5 class="title">{{ $product->title }}</h5>
                        </a>
                        <div class="details">{!! \Illuminate\Support\Str::words(strip_tags($product->description,'<br>'),20) !!}</div>

                        @if($product->for_request==0)
                            <p class="price">{{ count($product->variants) ? $product->variants[0]->price : $product->price}} AED</p>
                        @else
                            <p class="price">Price per request</p>
                        @endif
                    </div>
                    <div class="col-md-2">
                        <div class="addtocart">
                            @if($product->for_request)
                                <a class="view-variant" href="mailto:info@a--re.com?subject=ARE PRODUCT REQUEST&body=I would like to order this item: {{ $product->title }}">REQUEST</a>
                            @elseif(count($product->variants))
                                <a class="view-variant" href="{{ url('/product/'.$product['slug']) }}">View Variants</a>
                            @else
                                @if(Auth::user())
                                    <a href="#" class="add-to-cart" data-id="{{ $product->id }}">Add to Cart</a>
                                @else
                                    <a href="#" class="poplink add-to-cart" id="" data-target="login-panel">Add to Cart</a>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>

                {{--@foreach($product->attributes as $attribute)--}}
                {{--{{ $attribute->value }},--}}
                {{--@endforeach--}}
                
            </div>
    @endforeach
    {{ $products->links() }}
@else
    <div class="col-md-12">
        <p>No products found.</p>
    </div>
@endif
