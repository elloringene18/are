<div id="login-panel" class="overlay ">
    <div class="wrap"></div>
    <div class="panel">
        <a href="#" class="close-panel">X</a>
        <h2 class="main-font-color text-uppercase text-left">Welcome back!</h2>
        <h2 class="text-uppercase mb-4 text-left">log in to your account</h2>
        <form id="loginForm" action="{{url('customer-login')}}" method="post">
            @csrf
            <input type="text" class="form-control" placeholder="Email" name="email">
            <input type="password" class="form-control" placeholder="Password" name="password">
            <div class="form-group">
                <div class="form-error"></div>
            </div>
            <input type="submit" value="Login" class="form-control button mb-2">
        </form>
        <hr>
        <h3 class="text-uppercase text-center mt-3 mb-3">Forgot your password?</h3>
        <a href="#" class="button">Reset password</a>
        <hr>
        <h3 class="text-uppercase text-center mt-3 mb-3">Don't have an account?</h3>
        <a href="#" class="button poplink" data-target="register-panel">Create an account</a>
    </div>
</div>
