<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <title>ARE Studio</title>
    <meta name="description" content="ARE fuses tradition and modernity, with a twist, a unique reminder of our collective yesterdays and a promise of brighter tomorrows. Amongst a playful collision of materials, patterns and colors, of repetition and exploration – between the traditional and the contemporary.">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:title" content="ARE Studio">
    <meta property="og:type" content="">
    <meta property="og:url" content="">
    <meta property="og:image" content="">

    <link rel="manifest" href="{{ asset('public/') }}/site.webmanifest">
    <link rel="apple-touch-icon" href="{{ asset('public/') }}/icon.png">
    <!-- Place favicon.ico in the root directory -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="{{ asset('public/') }}/fonts/web/stylesheet.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('public/') }}/css/slick.min.css"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('public/') }}/css/bootstrap.min.css"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300;400;500;700&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('public/') }}/css/normalize.css">
    <link rel="stylesheet" href="{{ asset('public/') }}/css/main.css?v=1.4">
    <link rel="stylesheet" href="{{ asset('public/') }}/css/home.css">
    <link rel="stylesheet" href="{{ asset('public/') }}/css/login.css?v=1.1">
    <link href="{{ asset('public/') }}/fontawesome/css/all.css" rel="stylesheet">

    <meta name="theme-color" content="#fafafa">
    <style>
        body {
            font-family: 'Quicksand', sans-serif;
        }
    </style>

    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
    <style>

        #nav i {
            color: #fff;
        }

        #menu-bt {
            background-image: url('{{asset('public/img/menu-icon-white.png')}}');
        }

        #nav-logo {
            background-image: url('{{asset('public/img/are-logo-white.png')}}');
        }

        #home-slide {
            height: 100%;
        }

        .slick-track {
            display: flex;
        }

        .slick-track .slick-slide {
            display: flex;
            height: auto;
        }

        #top {
            height: 100vh !important;
            background-size: auto 100%;
        }


        #top .slide-1 {
            background-image: url('{{ asset('public/img/Space_1.jpg') }}');
            background-size: cover !important;
        }
        #top .slide-2 {
            background-image: url('{{ asset('public/img/Space_2.jpg') }}');
            background-size: cover !important;

        }
        #top .slide-3 {
            background-image: url('{{ asset('public/img/Space_3.jpg') }}');
            background-size: cover !important;

        }
        #top .slide-4 {
            background-image: url('{{ asset('public/img/Space_4.jpg') }}');
            background-size: cover !important;
        }
        #top .slide-5 {
            background-image: url('{{ asset('public/img/Space_5.jpg') }}');
            background-size: cover !important;

        }
        #top .slide-6 {
            background-image: url('{{ asset('public/img/Space_6.jpg') }}');
            background-size: cover !important;

        }
        #top .slide-7 {
            background-image: url('{{ asset('public/img/Space_7.jpg') }}');
            background-size: cover !important;

        }
        #top .slide-8 {
            background-image: url('{{ asset('public/img/Space_8.jpg') }}');
            background-size: cover !important;

        }

        .fade-in-image {
            animation: fadeIn 300ms;
            -webkit-animation: fadeIn 300ms;
            -moz-animation: fadeIn 300ms;
            -o-animation: fadeIn 300ms;
            -ms-animation: fadeIn 300ms;
        }

        @keyframes fadeIn {
            0% {opacity:0;}
            100% {opacity:1;}
        }

        @-moz-keyframes fadeIn {
            0% {opacity:0;}
            100% {opacity:1;}
        }

        @-webkit-keyframes fadeIn {
            0% {opacity:0;}
            100% {opacity:1;}
        }

        @-o-keyframes fadeIn {
            0% {opacity:0;}
            100% {opacity:1;}
        }

        @-ms-keyframes fadeIn {
            0% {opacity:0;}
            100% {opacity:1;}
        }

        .slide {
            position: absolute;
            height: 100%;
            width: 100%;
            background-position: center;
            background-size: auto 100%;
            top: 0;
            left: 0;
            display: none;
        }
        .slide.active {
            display: block;
        }

        @media only screen and (max-width:991px)  {

        }

        @media only screen and (max-width:620px)  {
            #top .slide-1 {
                background-image: url('{{ asset('public/img/home-bg3-mobile.jpg') }}');
            }
            #top .slide-2 {
                background-image: url('{{ asset('public/img/home-bg2-mobile.jpg') }}');
            }
            #top .slide-3 {
                background-image: url('{{ asset('public/img/home-bg-mobile.jpg') }}');
            }
        }
    </style>
    <link rel="stylesheet" href="{{ asset('public/') }}/css/shop.css">
</head>

<body>
@include('partials.login')
@include('partials.register')

<nav id="nav-wrap" class="">
    <ul>
        <li style=""><a href="{{ url('/') }}">HOME</a></li>
        <li style=""><a href="{{ url('/about-us') }}">ABOUT ARE</a></li>
        <li style=""><a href="{{ url('/collections') }}">COLLECTIONS</a></li>
        <li style=""><a href="{{ url('/shop') }}">PRODUCTS</a></li>
        <li style=""><a href="{{ url('/news') }}">PRESS RELEASES</a></li>
        <li style=""><a href="{{ url('/exhibitions') }}">EXHIBITIONS</a></li>
        <li style=""><a href="{{ url('/contact-us') }}">CONTACT US</a></li>
    </ul>
</nav>

<header id="nav">
    <div class="row">
        <div class="col-4 text-start">
            <a href="{{ url('/') }}" id="nav-logo"></a>
        </div>
        <div class="col-8 text-end right">
            <a href="#" id="menu-bt"></a>
            @if(Auth::user())
                <a href="{{ url('/account') }}"><i class="fas fa-user"></i></a>
                <a href="{{ url('/favourites') }}" class="wishlist"><i class="fas fa-heart"></i><span class="{{ count(Auth::user()->favourites) ? 'active' : '' }}">{{ count(Auth::user()->favourites) > 0 ? count(Auth::user()->favourites) : '' }}</span></a>
                <a href="{{ url('/cart') }}" class="cart"><i class="fas fa-shopping-cart"></i><span class="{{count(Auth::user()->cart) ? 'active' : ''}}">{{ count(Auth::user()->cart) > 0 ? count(Auth::user()->cart) : '' }}</span></a>
            @else
                <a href="#" class="account poplink" id="" data-target="login-panel"><i class="fas fa-user"></i></a>
                <a href="#" class="cart poplink" id="cart-bt" data-target="login-panel"><i class="fas fa-shopping-cart"></i></a>
                <a href="#" class="favourites poplink" data-target="login-panel"><i class="fas fa-heart"></i></a>
            @endif
        </div>
    </div>
</header>

<div id="top" class="">
    <div class="slide slide-1"></div>
    <div class="slide slide-2"></div>
    <div class="slide slide-3"></div>
    <div class="slide slide-4"></div>
    <div class="slide slide-5"></div>
    <div class="slide slide-6"></div>
    <div class="slide slide-7"></div>
    <div class="slide slide-8"></div>
</div>

{{--<div class="container mt-5">--}}
{{--    <div class="row">--}}
{{--        @include('partials.product-list')--}}

{{--        <div class="text-center col-md-12">--}}
{{--            <a href="{{ url('shop') }}" class="" id="" style="float:none">VIEW ALL PRODUCTS</a>--}}
{{--            <br/>--}}
{{--            <br/>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}

<div id="message-bubble"></div>

<script src="{{ asset('public/') }}/js/vendor/jquery-3.6.0.min.js"></script>
<script src="{{ asset('public/') }}/js/vendor/modernizr-3.11.2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/simple-parallax-js@5.2.0/dist/simpleParallax.min.js"></script>
<script type="text/javascript" src="{{ asset('public/') }}/js/vendor/slick.min.js"></script>
<script type="text/javascript" src="{{ asset('public/') }}/js/vendor/images-loaded.js"></script>
<script type="text/javascript" src="{{ asset('public/') }}/js/vendor/masonry.js"></script>
<script type="text/javascript" src="{{ asset('public/') }}/js/vendor/bootstrap.min.js"></script>
<script src="{{ asset('public/') }}/js/plugins.js"></script>
<script src="{{ asset('public/') }}/js/main.js"></script>
<script src="{{ asset('public/') }}/js/login.js"></script>

<script>

    var image = document.getElementsByClassName('step-down');

    new simpleParallax(image, {
        overflow: true,
        orientation: 'down'
    });

    var image = document.getElementsByClassName('step-up');

    new simpleParallax(image, {
        overflow: true,
        orientation: 'up'
    });

    var image = document.getElementsByClassName('step-left');

    new simpleParallax(image, {
        overflow: true,
        orientation: 'left'
    });

    var image = document.getElementsByClassName('step-right');

    new simpleParallax(image, {
        overflow: true,
        orientation: 'right'
    });

    var image = document.getElementsByClassName('step-right-full');

    new simpleParallax(image, {
        overflow: true,
        orientation: 'right',
        maxTransition: 99
    });
    // external js: masonry.pkgd.js, imagesloaded.pkgd.js

    var grid = document.querySelector('.grid');
    var msnry;

    $(document).ready(function(){

    });

    function masonry(){
        // init Isotope after all images have loaded
        msnry = new Masonry( grid, {
            itemSelector: '.grid-item',
        });
    }


</script>

<script>

    $('.poplink').on('click',function (e) {
        e.preventDefault();
        $('.overlay').removeClass('active');
        $('#'+$(this).attr('data-target')).toggleClass('active');
    });

    var baseUrl = '{{ url('/') }}';

    function getFavourites(){
        $.ajax({
            type: "GET",
            url: baseUrl+'/favourites/get-favourites/',
            success: function(response){
                $('#nav a.wishlist span').text(response);

                if(response > 0) {
                    $('#nav a.wishlist span').addClass('active');
                } else {
                    $('#nav a.wishlist span').removeClass('active');
                }

            },
            statusCode: {
                401: function() {
                    window.location.href = baseUrl+'/login'; //or what ever is your login URI
                }
            },
            complete : function (event,error){
            }
        });
    }

    function updateCart(){
        $.ajax({
            type: "GET",
            url: baseUrl+'/cart/get-count/',
            success: function(response){
                $('#nav a.cart span').text(response);

                if(response > 0) {
                    $('#nav a.cart span').addClass('active');
                } else {
                    $('#nav a.cart span').removeClass('active');
                }

            },
            statusCode: {
                401: function() {
                    window.location.href = baseUrl+'/login'; //or what ever is your login URI
                }
            },
            complete : function (event,error){
            }
        });
    }

    @if(Auth::user())
    getFavourites();
    updateCart();
    @endif

    $('#loginForm').on('submit',function (e) {
        e.preventDefault();
        e.stopPropagation();

        var form = $(this);

        e.preventDefault();
        e.stopPropagation();

        data = form.serialize();
        url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(response){

                if(response.success) {
                    form.find('.form-error').hide();
                    location.reload();
                } else {
                    form.find('.form-error').show().empty().append(response.message);
                }

            },
            statusCode: {
                401: function() {
                    window.location.href = '{{url('login')}}'; //or what ever is your login URI
                }
            },
            complete : function (event,error){
            }
        });
    });

    $('#registerForm').on('submit',function (e) {
        e.preventDefault();
        e.stopPropagation();

        var form = $(this);

        e.preventDefault();
        e.stopPropagation();

        data = form.serialize();
        url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(response){

                if(response.success) {
                    form.find('.form-error').hide();
                    location.reload();
                    // $('#registerSuccess').show().empty().append(response.message);
                } else {
                    form.find('.form-error').show().empty().append(response.message);
                }

            },
            statusCode: {
                401: function() {
                    window.location.href = '{{url('login')}}'; //or what ever is your login URI
                }
            },
            complete : function (event,error){
            }
        });
    });

    $('.add-to-cart').on('click',function (e) {
        e.preventDefault();
        bt = $(this);
        id = bt.attr('data-id');
        bt.text('ADDING...');

        $.ajax({
            type: "GET",
            url: baseUrl+'/cart/add/'+id,
            success: function(response){

                if(response.success) {
                    $('#message-bubble').show().empty().append(response.message);
                    bt.text('ADDED TO CART')
                } else {
                    form.find('.form-error').show().empty().append(response.message);
                }

            },
            statusCode: {
                401: function() {
                    window.location.href = '{{url('login')}}'; //or what ever is your login URI
                }
            },
            complete : function (event,error){
                updateCart();
            }
        });
    });

    $('.wish.clickable').on('click',function (e) {
        e.preventDefault();
        e.stopPropagation();

        id = $(this).attr('data-id');
        elem = $(this);
        $.ajax({
            type: "GET",
            url: baseUrl+'/favourites/toggle/'+id,
            success: function(response){
                if(response>0) {
                    elem.addClass('active');
                } else {
                    elem.removeClass('active');
                }
            },
            statusCode: {
            },
            complete : function (event,error){
                getFavourites();
            }
        });
    });

    $('#addToCartForm').on('submit',function (e) {
        e.preventDefault();
        e.stopPropagation();

        var form = $(this);

        bt = $(this).find('input[type=submit]').first();
        console.log(bt);
        bt.attr('value','ADDING...');

        data = form.serialize();
        url = form.attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(response){

                if(response.success) {
                    $('#message-bubble').show().empty().append(response.message);
                    bt.attr('value','ADDED TO CART').attr('disabled','disabled');
                } else {
                    form.find('.form-error').show().empty().append(response.message);
                }

            },
            statusCode: {
                401: function() {
                    window.location.href = '{{url('login')}}'; //or what ever is your login URI
                }
            },
            complete : function (event,error){
                updateCart();
            }
        });
    });


    $('#subscribeForm').on('submit',function (e) {
        e.preventDefault();
        e.stopPropagation();

        data = $(this).serialize();
        url = $(this).attr('action');

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            success: function(response){
                $('#subscribeSuccess').html(response).show();
                $('#subscribeForm').trigger('reset');
            }
        });

    });

</script>

<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>

<script>
    $(document).ready(function(){
        $('#featured-slide').slick({
            'autoplay' : true,
            'prevArrow' : '<i class="slick-prev arrow left"></i>',
            'nextArrow' : '<i class="slick-next arrow right"></i>',
        });
        $('#home-slide').slick({
            'autoplay' : true,
            'arrows' : false,
            'fade' : true
        });
    });

    var slide = 0;

    function changeSlide(){
        if(slide==0){
            $('.slide').eq(8).fadeOut();
            $('.slide').eq(0).fadeIn();
        } else if (slide==1){
            $('.slide').eq(0).fadeOut();
            $('.slide').eq(1).fadeIn();
        } else if (slide==2){
            $('.slide').eq(1).fadeOut();
            $('.slide').eq(2).fadeIn();
        }else if (slide==3){
            $('.slide').eq(2).fadeOut();
            $('.slide').eq(3).fadeIn();
        }else if (slide==4){
            $('.slide').eq(3).fadeOut();
            $('.slide').eq(4).fadeIn();
        }else if (slide==5){
            $('.slide').eq(4).fadeOut();
            $('.slide').eq(5).fadeIn();
        }else if (slide==6){
            $('.slide').eq(5).fadeOut();
            $('.slide').eq(6).fadeIn();
        }else if (slide==7){
            $('.slide').eq(6).fadeOut();
            $('.slide').eq(7).fadeIn();
        }else if (slide==8){
            slide=0
            $('.slide').eq(7).fadeOut();
            $('.slide').eq(0).fadeIn();
        }
        setTimeout(function () {
            changeSlide();
            ++slide;
        },5000);
    }
    changeSlide();
</script>

</body>

</html>
