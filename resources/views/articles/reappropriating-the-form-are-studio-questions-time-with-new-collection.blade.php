@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('public/css/articles.css') }}?v=1.2">
@endsection

@section('content')
    <section id="contents" class="container">
        <div class="clearfix">
            <div class="container" style="position: relative;">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Reappropriating The Form: ARE Studio Questions Time with New Collection</h1>
                        <hr/>
                        <div class="breadcrumbs">
                            <a href="{{ url('/') }}"> Home </a> <span class="divider">></span> <a href="{{ url('/news') }}"> News </a>
                        </div>
                        <div class="row mt-5">
                            <div class="col-md-11 offset-lg-1">
                                <div class="row mb-5">
                                    <div class="col-md-12">
                                        <img src="{{ asset('public/img/articles/jdeed.png') }}"  class="main-img">

                                        <p>&nbsp;</p>
                                        <p>Bridging the past, present, and future, ARE Studio releases a new collection of furniture pieces that interrogate the processes of tradition meeting modernity. Marked by glitches and intentional abstractions, their chairs, stools, tables, and mirrors are meant to transform spaces. ARE’s self-proclaimed playground is the sublime—a mixture of vibrant colors and mixed materials embedded, glitched imperfectly, into traditional Damascene-inspired furniture that breathes an ephemeral aura into furniture found in museums, palaces, and our grandmothers houses. The pieces in REAPPROPRIATING THE FORM are created through repetition, lines, and complex, almost impossible, alterations that create a frozen composition, at once stuck in time while transcending time itself.
                                        </p>
                                        <p>We caught up with the team behind ARE to learn more about their process and inspiration.
                                        </p>
                                        <p>&nbsp;</p>
                                        <p>By Ethan Dincer
                                        <p>&nbsp;</p>
                                        <img src="{{ asset('public/img/articles/jdeed1.png') }}"><p>&nbsp;</p>
                                        <h4 class="center">CTRL-P SEAT</h4>
                                        <p class="center">Material: Walnut wood/Mother of Pearl Size: W96 x H115
                                        <p>&nbsp;</p>
                                        <h4>HOW HAVE YOUR PERSONAL IDENTITIES AND EXPERIENCES SHAPED THE PIECES IN REAPPROPRIATING THE FORM? WHY DID YOU CHOOSE THE FURNITURE PIECES YOU DID TO REINTERPRET AND REIMAGINE?</h4>
                                        <p>Our pieces are inspired by Damascus, a city that is as old as time, that has been shaped by different civilisations. It is this melting pot of cultures, customs and experiences that formed the foundation upon which ARE was built. We draw from the traditional and mold and shape our own experiences into every piece, each time creating something unique. We have handpicked traditional damascene chairs, mirrors, tables, and reimagined them with elements of modern architecture.</p>
                                        <p>“Our pieces are for those people who appreciate the past, live in the present and look to the future. “<p>
                                        <p>&nbsp;</p>
                                        <h4>HOW, IF AT ALL, HAS THE PANDEMIC AND A RECKONING WITH THE PAST AND FUTURE DURING LOCKDOWNS INSPIRED THIS COLLECTION’S DESIRE TO FUSE TRADITION AND MODERNITY? GIVEN THAT THE WORLD, AT LEAST FOR NOW, IS SLOWLY OPENING, HOW DOES THIS COLLECTION WORK IN TANDEM WITH A GLOBAL DESIRE FOR A BRIGHTER TOMORROW?</h4>
                                        <p>Our pieces are inspired from bits and pieces of our lives, memories from our childhood blended into adult experiences. It’s these collective moments, both the good and the bad, that form the core of each piece we create. The pandemic has brought chaos and uncertainty to so many aspects of our lives, it has created new norms, and in many ways this is what ARE is about—creating new norms from old practices.</p>

                                        <p>&nbsp;</p>
                                        <img src="{{ asset('public/img/articles/jdeed2.png') }}"><p>&nbsp;</p>
                                        <h4 class="center">WHAT YOU SEE ISNT ALL THERE IS MIRROR</h4>
                                        <p class="center">Material: Wood/Mother of Pearl/Neon Size: W130cm x H280cm</p>
                                        <p>&nbsp;</p>
                                        <h4>HOW DID YOU DECIDE ON MATERIALS AND ELEMENTS TO GLITCH OR ABSTRACT? WHAT GOES INTO ARE’S PROCESS AND DESIGN?</h4>
                                        <p>Damascene furniture is mainly made from wood and mother of pearl—so we made sure to use those two elements liberally while combining newer materials such as epoxy, plexi, and neon. That said, we have quite a few different craftsmen and people we allow to get involved in the production process, which means that no piece is ever the same—all the different hands contributing lead to glitches and errors that makes each piece unique.</p>
                                        <p>For designing, we put a lot of effort into researching, creating our concept and mood board, sketching, testing different materials, and designing the final piece. We start with the traditional methods—because we respect our roots and use it as a foundation to inform the piece—but we also make sure to incorporate plenty of newer materials and methods as we expand from the base. By the end of the design phase, we look back and make sure we’ve succeeded in challenging norms and changing our attitudes in the creation of the piece as we reimagine it.</p>
                                        <p>&nbsp;</p>
                                        <h4>WHO DO YOU IMAGINE YOUR PIECES TO BE FOR? WHAT GOES INTO CONSIDERING AUDIENCE AND USABILITY?</h4>
                                        <p>Our pieces are for those people who appreciate the past, live in the present and look to the future.  They are art pieces that can stand the test of time—old or new, young and old, ARE is a reflection each one of us.</p>

                                        <p>&nbsp;</p>
                                        <img src="{{ asset('public/img/articles/jdeed3.png') }}"><p>&nbsp;</p>
                                        <h4 class="center">LOVE MIRROR</h4>
                                        <p class="center">Material: Wood/Mother of Pearl/Plexi Glass/Neon Size: W130cm x H280cm</p>
                                        <p>&nbsp;</p>

                                        <p>Cover picture:</p>
                                        <h4>CYMK END TABLE</h4>
                                        <p>Material: Wood/Mother of Pearl Size: W52cm x H65cm</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('articles.other-news-1')
            </div>
        </div>
    </section>
@endsection

@section('js')

    <script>
        $('.articles').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            centerMode: false,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        arrows: false,
                        dots: false,
                        autoplay: true
                    }
                }
            ],
        });
    </script>
@endsection
