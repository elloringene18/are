@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('public/css/articles.css') }}?v=1.2">
@endsection

@section('content')
    <section id="contents" class="container">
        <div class="clearfix">
            <div class="container" style="position: relative;">
                <div class="row">
                    <div class="col-md-12">
                        <h1>The 9 Emerging Middle Eastern Designers To Watch</h1>
                        <hr/>
                        <div class="breadcrumbs">
                            <a href="{{ url('/') }}"> Home </a> <span class="divider">></span> <a href="{{ url('/news') }}"> News </a>
                        </div>
                        <div class="row mt-5">
                            <div class="col-md-11 offset-lg-1">
                                <div class="row mb-5">
                                    <div class="col-md-12">
                                        <img src="{{ asset('public/img/articles/news-5.jpg') }}" class="main-img">
                                        <br/>
                                        <p>Memory, tradition and modernity are the cornerstones of this Dubai and Syria-based studio making its mark with playful and exuberant objects that offer a refreshing twist on heritage. Unexpected materials, bright colours and a sense of humour hide the inherent complexity of their designs which are born from collaborations with an array of local craft communities and manipulating classical typography.</p>
                                        <br/>
                                        <p>Source: <a href="https://www.admiddleeast.com/art-design/design/new-designers-in-the-middle-east-ad-50" target="_blank">Architectural Digest</a> </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('articles.other-news-1')
            </div>
        </div>
    </section>
@endsection

@section('js')

    <script>
        $('.articles').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            centerMode: false,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        arrows: false,
                        dots: false,
                        autoplay: true
                    }
                }
            ],
        });
    </script>
@endsection
