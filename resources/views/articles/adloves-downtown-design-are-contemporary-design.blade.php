@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('public/css/articles.css') }}?v=1.2">
@endsection

@section('content')
    <section id="contents" class="container">
        <div class="clearfix">
            <div class="container" style="position: relative;">
                <div class="row">
                    <div class="col-md-12">
                        <h1>#ADLoves ARE’s Bold And Bright Take On Tradition</h1>
                        <hr/>
                        <div class="breadcrumbs">
                            <a href="{{ url('/') }}"> Home </a> <span class="divider">></span> <a href="{{ url('/news') }}"> News </a>
                        </div>
                        <div class="row mt-5">
                            <div class="col-md-11 offset-lg-1">
                                <div class="row mb-5">
                                    <div class="col-md-12">
                                        <img src="{{ asset('public/img/articles/adlove.jpg') }}" class="main-img">
                                        <p>&nbsp;</p>
                                        <p>The exciting new atelier unveiled their unique interpretations of heritage designs at Downtown Design 2021.</p>
                                        <p>&nbsp;</p>
                                        <p>by AD Staff</p>
                                        <p>&nbsp;</p>
                                        <img src="{{ asset('public/img/articles/adlove1.jpg') }}">
                                        <p>&nbsp;</p>
                                        <p>Memory, tradition and modernity are the cornerstones of ARE, a studio making its mark with playful and exuberant pieces that offer a refreshing twist on heritage. Unexpected materials, bright colours and a sense of humour hide the inherent complexity of their designs that are born from manipulating classical typography to lend a bold and dynamic spirit to patterns, compositions and objects that might seem familiar, but, are anything but.</p>
                                        <p>&nbsp;</p>
                                        <img src="{{ asset('public/img/articles/adlove2.jpg') }}">
                                        <p>&nbsp;</p>
                                        <p>#ADLoves: Discover 12 Brands Making A Splash At Downtown Design 2021</p>
                                        <p>&nbsp;</p>
                                        <img src="{{ asset('public/img/articles/adlove3.jpg') }}">
                                        <p>&nbsp;</p>
                                        <p>Surreal, unexpected and oh, so exciting, their creations are a work of deconstruction and re-building. Designed and crafted to the highest standards, objects such as chairs, mirrors and tables that speak of rich traditions and materiality are at the centre of their work. Working with master craftspeople and avant-garde makers, the atelier carefully takes apart each component, adding back vibrant colour through sleek plexiglass panels and neon, reimaging olden designs for a new generation.</p>
                                        <p>&nbsp;</p>
                                        <img src="{{ asset('public/img/articles/adlove4.jpg') }}">
                                        <p>&nbsp;</p>
                                        <p>An artful combination of repetition, linear design serves as the opposite of the richly layered traditional elements; at first glance the objects look like they were digitally manipulated, when in fact they are a 100 percent real. Visit brand website here.</p>
                                        <p>&nbsp;</p>
                                        <p>Find our more about Downtown Design 2021: downtowndesign.com</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('articles.other-news-1')
            </div>
        </div>
    </section>
@endsection

@section('js')

    <script>
        $('.articles').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            centerMode: false,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        arrows: false,
                        dots: false,
                        autoplay: true
                    }
                }
            ],
        });
    </script>
@endsection
