<div class="row">
    <div class="col-md-12">
        <h1>Other News</h1>
        <div class="articles">
            <div>
                <a href="{{ url('news/reappropriating-the-form-are-studio-questions-time-with-new-collection') }}"><img src="{{ asset('public/img/articles/jdeed-thumb.jpg') }}" class="mb-3" width="100%"></a>
                <a href="{{ url('news/reappropriating-the-form-are-studio-questions-time-with-new-collection') }}"><h5>Reappropriating The Form: ARE Studio Questions Time with New Collection</h5></a>
            </div>
            <div>
                <a href="{{ url('news/al-beit-magazine') }}"><img src="{{ asset('public/img/articles/albeit-thumb.jpg') }}" width="100%" class="mb-3"></a>
                <a href="{{ url('news/al-beit-magazine') }}"><h5>Al Beit Magazine</h5></a>
            </div>
            <div>
                <a href="{{ url('news/architectural-digest') }}"><img src="{{ asset('public/img/articles/news-6-thumb.jpg') }}" width="100%" class="mb-3"></a>
                <a href="{{ url('news/architectural-digest') }}"><h5>Architectural Digest 50</h5></a>
            </div>
        </div>
    </div>
</div>
