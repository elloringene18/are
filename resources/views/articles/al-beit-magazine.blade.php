@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('public/css/articles.css') }}?v=1.2">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/2.5.0-beta.2/css/lightgallery.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/2.5.0-beta.2/css/lg-fullscreen.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/2.5.0-beta.2/css/lg-zoom.min.css">
    <style>
        .im {
            display: block;
            width: auto;
            float: left;
            position: relative;
        }
        .im:after {
            width: 100%;
            position: absolute;
            background-color: rgba(0,0,0,0.3);
            display: none;
            height: 100%;
            background-image: url({{asset('public/img/zoom.png')}});
            background-position: center;
            background-repeat: no-repeat;
            background-size: 50px;
        }
        .im:hover:after {
            display: block;
        }
    </style>
@endsection

@section('content')
    <section id="contents" class="container">
        <div class="clearfix">
            <div class="container" style="position: relative;">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Al Beit Magazine</h1>
                        <hr/>
                        <div class="breadcrumbs">
                            <a href="{{ url('/') }}"> Home </a> <span class="divider">></span> <a href="{{ url('/news') }}"> News </a>
                        </div>
                        <div class="row mt-5">
                            <div class="col-md-11 offset-lg-1">
                                <div class="row mb-5">
                                    <div class="col-md-12">
                                        <div id="lightgallery">
                                            <a href="{{ asset('public/img/articles/albeit.jpg') }}" data-lg-size="1600-2400">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="im clearfix">
                                                            <img src="{{ asset('public/img/articles/albeit.jpg') }}" width="300" style="width: 300px !important; float: left">
                                                        </div>
                                                    </div>
                                                </div>
                                                Click to zoom
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @include('articles.other-news-1')
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/2.5.0-beta.2/lightgallery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/2.5.0-beta.2/plugins/zoom/lg-zoom.min.js"></script>
    <script>
        $('.articles').slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3,
            centerMode: false,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        arrows: false,
                        dots: false,
                        autoplay: true
                    }
                }
            ],
        });

        lightGallery(document.getElementById('lightgallery'), {
            plugins: [lgZoom],
            speed: 500,
        });
    </script>
@endsection
