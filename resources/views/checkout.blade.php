@extends('master')

@section('css')
    <link href="{{ asset('public/') }}/css/cart.css" rel="stylesheet">
    <style>
        .product-name, .price {
            font-size: 14px;
        }
    </style>
@endsection

@section('content')
    @inject('locationService', 'App\Services\LocationService')
    <?php $countries = $locationService->getAllCountriesWithId(); ?>

    <section id="page" class="inner-page">
        <div class="container">
            <form action="{{ url('cart/checkout') }}" method="post">
            {{--<form action="" method="post">--}}
                @csrf()
                <div class="row relative">
                    <h1 class="">Checkout</h1>
                </div>
                <div class="row relative">
                    <div class="col-lg-6 col-md-6 col-sm-12 pb-4 pt-4 cart">
                        <h5>Delivery Address</h5>
                            <div class="form-group">
                                <label>Full Name</label>
                                <input type="text" name="profile[name]" class="form-control" value="" required>
                            </div>
                            <div class="form-group">
                                <label>Mobile Number</label>
                                <input type="text" name="profile[mobile]" class="form-control" value="" required>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" name="profile[email]" class="form-control" value="" required>
                            </div>
                            <div class="form-group">
                                <label>Country</label>
                                <select name="address[country]" class="form-control" id="country">
                                    @foreach($countries as $country)
                                        <option {{$country->name=="United Arab Emirates" ? 'selected' : ''}} data-id="{{$country->id}}" value="{{$country->name}}">{{$country->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>City/State</label>
                                <select name="address[city]" class="form-control" id="city"></select>
                            </div>
                            <div class="form-group">
                                <label>Street</label>
                                <input type="text" class="form-control" name="address[street]" value="" required/>
                            </div>
                            <div class="form-group">
                                <label>Building Name</label>
                                <input type="text" class="form-control" name="address[building]" value="" required/>
                            </div>
                            <div class="form-group">
                                <label>Apartment/Flat No.</label>
                                <input type="text" class="form-control" name="address[apartment]" value="" required/>
                            </div>
                        {{--<p class="mt-5 terms">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis. </p>--}}
                    </div>

                    <?php
                    $subtotal = 0;
                    $total = 0;
                    $shipping = 0;

                    foreach($data as $item){
                        $subtotal += $item->product_variant_id ? $item->variant->price * $item->qty : $item->product->price * $item->qty;
                    }

                    $total = $subtotal + $shipping;
                    ?>


                    <div class="col-lg-6 col-md-6 col-sm-12 pb-4 pt-4">
                        <div class="totals">
                            <table width="100%" class="mb-4">
                                <th>
                                    <tr>
                                        <td>Product</td>
                                        <td>Total</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><hr/></td>
                                    </tr>
                                </th>
                                @foreach($data as $item)
                                    <tr>
                                        <td  class="">
                                            <p class="product-name">{{ $item->product->title }} | x{{ $item->qty }}</p>
                                        </td>
                                        <td  class=""><p class="price">{{ $item->product_variant_id ? $item->variant->price * $item->qty : $item->product->price * $item->qty }} AED</p></td>
                                    </tr>
                                @endforeach
                            </table>
                            <hr/>
                            <h1 class="mb-4">Total</h1>
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="label">Sub Total</span> <span class="value"><span class="price">{{$subtotal}} AED</span></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <br/>
                                    <span class="label"><strong>Delivery fee is not included.</strong></span> <span class="value  price"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    {{--UNDER MAINTENANCE--}}
                                    <input type="submit" class="checkout mt-5" value="Checkout" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>

@endsection

@section('js')
    <script>
        $('#country').on('change',function (){
            id = $(this).find('option:selected').attr('data-id');

            $.ajax({
                type: "GET",
                url: baseUrl+"/get-cities-for-country/"+id,
                async: false,
                cache: false,
                contentType: false,
                processData: false,
                success: function(response){
                    console.log(response);
                    $('#city').find('option').remove();


                    Object.keys(response).map(function(objectKey, index) {
                        var value = response[objectKey];
                        $('#city').append('<option value="'+value.name+'">'+value.name+'</option>');
                    });
                },
                statusCode: {
                    401: function() {
                        window.location.href = '{{url('login')}}'; //or what ever is your login URI
                    }
                },
                complete : function (event,error){
                }
            });
        });

        $('#country').trigger('change');
    </script>
@endsection


