@extends('master')

@section('css')
    <style>
        #top {
            background-image: url('{{ asset('public') }}/img/about-bg.png');
        }

        .form .left {
            border-right: 1px solid #15921E;
            padding-right: 90px;
        }

        .form .right {
            padding-left: 60px;
        }
        
        .heading {
            color: #15921E;
        }

        .tab-content {
            display: none;
            padding-left: 60px;
        }

        .tab-content.active {
            display: block;
        }

        .tab-nav {
            padding: 0;
        }

        .tab-nav li {
            background-image: url('{{ asset('public/img/arrow-right.png') }}');
            background-repeat: no-repeat;
            background-position: right center;
            height: 42px;
            line-height: 42px;
            width: 100%;
            display: block;
            border-bottom: 1px solid #707070;
            cursor: pointer;
            padding-right: 30px;
        }

        .tab-nav li.active {
            border-bottom: 1px solid #15921E;
        }

        .tab-nav li a {
            color: #000;
            text-decoration: none;
        }

        .tab-nav li.active a {
            color: #15921E;
        }

        @media only screen and (max-width:767px) {
            .form .left {
                padding-right: 0;
                border: 0;
            }
            .form .right {
                padding-left: 0;
                margin-bottom: 60px;
            }

            .tab-content {
                padding-left: 0;
            }
        }

    </style>
@endsection

@section('content')
    <section id="contents" class="container">
        <div class="clearfix">
            <div class="container form" style="position: relative;">
                <h1 class="top-title">CONTACT US</h1>
                <div class="row">
                    <div class="col-md-8">
                        <div class="left">
                            <form class="contact" action="{{url('contact')}}" method="post">
                                @if(Session::has('message'))
                                    <div class="alert alert-success">{{ Session::get('message') }}</div>
                                @endif
                                @if(Session::has('error'))
                                    <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                @endif
                                @csrf
                                    <label>Name</label>
                                    <input class="form-control" name="name" placeholder="Enter your name">
                                    <label>Email</label>
                                    <input class="form-control" name="email" placeholder="Enter your email">
                                    <label>Comments</label>
                                    <textarea class="form-control" name="message" placeholder="Enter your message"></textarea>
                                    <div class="text-end">
                                        <input type="submit" value="Submit">
                                    </div>
                                </form>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="right">
                            <p class="heading">E-mail</p>
                            <p>info@a--re.com</p>
                            <br/>
<!--
                            <p class="heading">Address</p>
                            <p>Lorem ipsum dolor</p>
-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="container" style="position: relative;">
                <div class="row">
                    <h1>Terms and conditions </h1>
                    <p>Please read these Terms and Conditions (“Terms”, “Terms and Conditions”) carefully before using the https://aretradinos.netlify.app/ website (the “Service”) operated by ARE (“us”, “we”, or “our”).
                        Your access to and use of the Service is conditioned on your acceptance of and compliance with these Terms. These Terms apply to all visitors, users and others who access or use the Service.
                    </p>
                    <div class="mt-3 row">
                        <div class="col-md-4">
                            <ul class="tab-nav">
                                <li class="active"><a href="#">Purchases</a></li>
                                <li><a href="#">Availability, errors and inaccuracies</a></li>
                                <li><a href="#">Accounts</a></li>
                                <li><a href="#">Links to other web sites</a></li>
                                <li><a href="#">Termination</a></li>
                                <li><a href="#">Governing law</a></li>
                                <li><a href="#">Orders</a></li>
                                <li><a href="#">Shipping policy</a></li>
                                <li><a href="#">Shipping costs and delivery estimates</a></li>
                                <li><a href="#">Shipping confirmation and order tracking</a></li>
                                <li><a href="#">Customs duties and taxes</a></li>
                                <li><a href="#">Returns and refunds </a></li>
                                <li><a href="#">Privacy policy </a></li>
                                <li><a href="#">Information collection and use</a></li>
                                <li><a href="#">Information sharing and disclosure</a></li>
                                <li><a href="#">Confidentiality and Security </a></li>
                                <li><a href="#">Cookies </a></li>
                                <li><a href="#">Preferences</a></li>
                                <li><a href="#">Changes to this privacy policy</a></li>
                            </ul>
                        </div>
                        <div class="col-md-8">
                            <div class="tab-content active">
                                <p><strong>Purchases</strong></p>
                                <p>By accessing or using the Service you agree to be bound by these Terms. If you disagree with any part of the terms then you may not access the Service.</p>
                                <p>If you wish to purchase any product or service made available through the Service, you may be asked to supply certain information relevant to your Purchase including, without limitation, your credit card number, the expiration date of your credit card, your billing address, and your shipping information.</p>
                                <p>You represent and warrant that:</p>
                                <ul>
                                    <li>You have the legal right to use any credit card(s) or other payment method(s) in connection with any Purchase; and that</li>
                                    <li>The information you supply to us is true, correct and complete to the best of your knowledge.</li>
                                </ul>
                                <p>By submitting such information, you grant us the right to provide the information to third parties for purposes of facilitating the completion of Purchases.</p>
                                <p>We reserve the right to refuse or cancel your order at any time for certain reasons including but not limited to: product or service availability, errors in the description or price of the product or service, error in your order or other reasons including when fraud or an unauthorized or illegal transaction is suspected.</p>
                            </div>
                            <div class="tab-content">
                                <p><strong>Availability, errors and inaccuracies</strong></p>
                                <p>We are constantly updating our products and services. We cannot and do not guarantee the accuracy or completeness of any information, including prices, product images, specifications, availability, and services. We reserve the right to change or update information and to correct errors, inaccuracies, or omissions at any time without prior notice.</p>
                            </div>
                            <div class="tab-content">
                                <p><strong>Accounts</strong></p>
                                <p>When you create an account with us, you must provide us information that is accurate, complete, and current at all times. Failure to do so constitutes a breach of the Terms, which may result in immediate termination of your account on our Service.</p>
                                <p>You are responsible for safeguarding the password that you use to access the Service and for any activities or actions under your password, whether your password is with our Service or a third-party service.</p>
                                <p>You agree not to disclose your password to any third party and inform us when you become aware of any breach of security or unauthorized use of your account.</p>
                                <p>You may not use as a username the name of another person or entity or that is not lawfully available for use, a name or trade mark that is subject to any rights of another person or entity other than you without appropriate authorization, or a name that is otherwise offensive, vulgar or obscene.</p>
                            </div>
                            <div class="tab-content">
                                <p><strong>Links to other web sites</strong></p>
                                <p>Our Service may contain links to third-party websites or services that are not owned or controlled by ARE.</p>
                                <p>ARE has no control over, and assumes no responsibility for, the content, privacy policies, or practices of any third-party web sites or services. You further acknowledge and agree that ARE shall not be responsible or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in connection with use of or reliance on any such content, goods or services available on or through any such web sites or services.
                                </p>
                            </div>
                            <div class="tab-content">
                                <p><strong>Termination</strong></p>
                                <p>We may terminate or suspend your account immediately, without prior notice or liability, for any reason whatsoever, including without limitation if you breach the Terms.</p>
                                <p>Upon termination, your right to use the Service will immediately cease. If you wish to terminate your account, you may simply discontinue using the Service.</p>
                            </div>
                            <div class="tab-content">
                                <p><strong>Governing law</strong></p>
                                <p>Our failure to enforce any right or provision of these Terms will not be considered a waiver of those rights. If any provision of these Terms is held to be invalid or unenforceable by a court, the remaining provisions of these Terms will remain in effect. These Terms constitute the entire agreement between us regarding our Service, and supersede and replace any prior agreements we might have between us regarding the Service.</p>
                            </div>
                            <div class="tab-content">
                                <p><strong>Orders</strong></p>
                                <p>All orders are subject to acceptance and availability, and items in your shopping basket are not reserved and may be purchased by other customers. Once your order has been placed, you will receive an email acknowledging the details of your order. This email is NOT an acceptance of your order, just a confirmation that we have received it.</p>
                                <p>We reserve the right not to accept your order and/or process a transaction for any reason or refuse service to anyone at any time at our sole discretion. We will not be liable to you or any third party by reason of our withdrawing any merchandise from the Site whether or not that merchandise has been sold, removing, screening, or editing any materials or content on the Site, refusing to process a transaction or unwinding or suspending any transaction after processing has begun.
                                    From time to time, prices are subject to change in response to currency exchange rate changes, markdowns, and other commercial factors. The price applicable to your order will be the price current at the time your order is accepted.
                                </p>
                            </div>
                            <div class="tab-content">
                                <p><strong>Shipping policy</strong></p>
                                <p>As the client, you are expected to cover shipping costs for all returns. The shipping costs are non-refundable. Returns are allowed and refunded. However, the customer has to handle all shipping fees. All refunds are issued minus return shipping costs</p>
                            </div>
                            <div class="tab-content">
                                <p><strong>Shipping costs and delivery estimates</strong></p>
                                <p>All charges for your order are calculated and sent to you for approval before shipping. Please review all pricing and cost estimates and notify ARE of any pricing errors or shipping mistakes.	</p>
                            </div>
                            <div class="tab-content">
                                <p><strong>Shipping confirmation and order tracking</strong></p>
                                <p>Once your order is shipped, a shipping confirmation email containing your tracking number (s) will be sent to your provided email address. This number is only active within 24 hours.
                                    </p>
                                <p>Our shipping prices do not include tax duties or customs pertaining to the destination country. These taxes will be paid by the customer.
                                </p>
                            </div>
                            <div class="tab-content">
                                <p><strong>Customs duties and taxes</strong></p>
                                <p>ARE is not responsible for any customs and taxes on your order. It is the customers’ sole responsibility to cover all fees relating to tariffs, taxes, etc. </p>
                            </div>
                            <div class="tab-content">
                                <p><strong>Returns and refunds </strong></p>
                                <p>You have 30 calendar days from the date of delivery to return an item. For a return to be accepted, it must be unused, in the condition sold in, in its original packaging, and accompanied by the receipt or equivalent proof of purchase.
                                   </p>
                                <p> Once ARE receives your returned item, you will be duly notified of the status of your refund once the item is inspected. If approved, a refund to your credit card (original payment method) will be initiated.
                                    </p>
                                <p>The refund will reflect on your card or account, depending on your payment processors’ policies. Customized items cannot be returned or exchanged.
                                </p>
                            </div>
                            <div class="tab-content">
                                <p><strong>Privacy policy  </strong></p>
                                <p>ARE respects and protects the privacy of the individuals that use ARE, as explained in this privacy policy (“Privacy Policy”)
                                    </p>
                                <p>Please read this Privacy Policy carefully. If you do not agree with the terms of this Privacy Policy, do not access or use the site. By accessing or using the Site, you agree to this Privacy Policy. The Privacy Policy may change from time to time. ARE will post the modified version at this same URL. Your continued use of the Site after the Privacy Policy has been modified is deemed an acceptance of those modifications.
                                   </p>
                                <p> This Privacy Policy does not apply to information collected by third parties, including websites that may be linked to from this Site. ARE is not responsible for the content or privacy practices of any non-ARE website to which this site links.
                                </p>
                            </div>
                            <div class="tab-content">
                                <p><strong>Information collection and use</strong></p>
                                <p>ARE does not collect any unique information about you (such as your name, email address, etc.) except when you specifically and knowingly provide such information. ARE collects personal information and information about your transactions when you use ARE services, such as buying items.
                                </p>
                                <p>When you register, ARE asks for your email address and any information that identifies you. Any additional information is requested when you purchase an item, such as name, address, and phone number. Once you register with ARE and sign in, you are no longer anonymous to ARE.
                                </p>
                                <p>The ARE server logs automatically receive and record information from your browser, including your IP address, ARE cookie information, the page requested, and other standard information.
                                </p>
                                <p></p>
                            </div>
                            <div class="tab-content">
                                <p><strong>Information sharing and disclosure</strong></p>
                                <p>Your email address and other personal information are kept private. ARE does not rent, sell, or share personal information about you with other people or companies. ARE may divulge aggregate information about users but will not share personally identifiable information with any third party without your express consent.
</p>
                                <p>
                                    For example, ARE may disclose how frequently the average ARE user visits ARE, or which sort of art is most popular. ARE will only release specific personal information about you if required to do so in order to comply with a valid legal process such as a search warrant, subpoena, statute, or court order, or if ARE believes it is necessary to share information in order to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of the Terms of Service, or as otherwise required by law.
                                </p>
                            </div>
                            <div class="tab-content">
                                <p><strong>Confidentiality and Security </strong></p>
                                <p>ARE limits access to personal information about you to employees who ARE believe reasonably need to come into contact with that information in order to do their jobs. ARE has physical, electronic, and procedural safeguards that comply with federal regulations to protect personal information about you. Your ARE account information is secure, protected by a password, or via an email login link.</p>
                            </div>
                            <div class="tab-content">
                                <p><strong>Cookies </strong></p>
                                <p>ARE uses cookies to remember your shopping cart, to keep you signed in, and various tasks related to ARE. ARE will not disclose cookies to third parties except as required by a valid legal process such as a search warrant, subpoena, statute, or court order.</p>
                            </div>
                            <div class="tab-content">
                                <p><strong>Preferences </strong></p>
                                <p>ARE reserves the right to send you certain important communications relating to ARE, such as service announcements and administrative messages, which are considered critical to ARE membership, without offering you the opportunity to opt-out of receiving them.</p>
                            </div>
                            <div class="tab-content">
                                <p><strong>Changes to this privacy policy </strong></p>
                                <p>By using ARE, you consent to the collection and use of your information as outlined in this policy and to the Terms & Conditions. ARE may update this Privacy Policy from time to time. The most current version of the Privacy Policy can always be found at <a href="https://aretradinos.netlify.app/privacypolicy" target="_blank">https://aretradinos.netlify.app/privacypolicy</a>.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script>
        $('.tab-nav li').on('click',function(e){
            e.preventDefault();
            indx = $('.tab-nav li').index($(this));
            console.log(indx);

            $('.tab-content').removeClass('active');
            $('.tab-nav li').removeClass('active');
            $(this).addClass('active');
            $('.tab-content').eq(indx).addClass('active');
        });
    </script>
@endsection
