@extends('master')

@section('css')

    <link rel="stylesheet" href="{{ asset('public/bigpicture/css/main.css') }}" />
    <style>
        {{--#top {--}}
            {{--background-image: url('{{ asset('public') }}/img/about-bg.png');--}}
            {{--height: 100vh;--}}
        {{--}--}}

        #contents p {
            margin-bottom: 4px;
        }

        #contents {
            padding-top: 100px;
            min-height: 800px;
        }

        .htmlvid {
            position: absolute;
            width: 100%;
            height: 100%;
            background-color: rgba(0,0,0,0.3);
            border: 0;
            left: 0;
            background-image: url('{{ asset('public/videos/playbt.png') }}');
            background-repeat: no-repeat;
            background-size: 50px;
            background-position: center;
            display: none;
            top: 0;
        }

        .vid {
            position: relative;
        }

        .vid:hover .htmlvid {
            display: block;
        }

        h5 {
            margin: 10px 0 20px;
        }
    </style>
@endsection

@section('content')
    <section id="contents" class="container">
        <div class="clearfix">
            <div class="container" style="position: relative;">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Exhibitions</h1>
                        <hr/>
                        <div class="mt-5">
                            <div class="col-md-4 ">
                                <div class="vid">
                                    <img src="{{ asset('public/videos/exhibition.jpg') }}" width="100%">
                                    <button class="htmlvid" vidSrc="{{ asset('public/videos/1-opt.mp4') }}"></button>
                                </div>
                                <h5>Dubai Design District</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('public/bigpicture/js/BigPicture.js') }}"></script>
    <script>

        $('.htmlvid').on('click',function(e){
            BigPicture({
                el: e.target,
                vidSrc: e.target.getAttribute('vidSrc')
            });
        });

    </script>
@endsection
