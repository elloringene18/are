@extends('master')

@section('css')
    <style>
        {{--#top {--}}
            {{--background-image: url('{{ asset('public') }}/img/about-bg.png');--}}
            {{--height: 100vh;--}}
        {{--}--}}

        #contents p {
            margin-bottom: 4px;
        }

        #contents {
            padding-top: 100px;
        }

        .add-to-cart, .view-variant {
            color: #03930F;
            border: 1px solid #03930F;
            width: auto;
            text-align: center;
            display: block;
            float: left;
            font-weight: bold;
            line-height: 16px;
            font-size: 12px;
            transition: background-color 300ms;
            margin-top: 30px;
            padding: 14px 10px;
        }

        .add-to-cart:hover, .view-variant:hover {
            color: #fff;
            background-color: #03930F;
        }

        @media only screen and (max-width: 620px){
            #contents h1 {
                font-size: 30px;
            }
        }

    </style>
@endsection

@section('content')
    <section id="contents" class="container">
        <div class="clearfix">
            <div class="container" style="position: relative;">
                <div class="row">
                    <div class="col-md-12">
                        <h1>Press Releases</h1>
                        <hr/>
                        <div class="row mt-5">
                            <div class="col-md-11 offset-lg-1">
                                <div class="row mb-5">
                                    <div class="col-md-4">
                                        <a href="{{ url('news/the-9-emerging-middle-eastern-designers-to-watch') }}"><img src="{{ asset('public/img/articles/news-5-thumb.jpg') }}" class="mb-3" width="100%"></a>
                                    </div>
                                    <div class="col-md-8">
                                        <a href="{{ url('news/the-9-emerging-middle-eastern-designers-to-watch') }}"><h3>The 9 Emerging Middle Eastern Designers To Watch</h3></a>
                                        <p>Memory, tradition and modernity are the cornerstones of this Dubai and Syria-based studio making its mark with playful and exuberant objects that offer a refreshing twist on heritage. Unexpected materials, bright colours and a sense of humour hide the inherent complexity of their designs which are born from collaborations ...</p>
                                        <a class="view-variant" href="{{ url('news/the-9-emerging-middle-eastern-designers-to-watch') }}">Read more</a>
                                    </div>
                                </div>
                                <div class="row mb-5">
                                    <div class="col-md-4">
                                        <a href="{{ url('news/the-best-designers-in-the-middle-east-ad50-2022-revealed') }}"><img src="{{ asset('public/img/articles/news-4-thumb.jpg') }}" class="mb-3" width="100%"></a>
                                    </div>
                                    <div class="col-md-8">
                                        <a href="{{ url('news/the-best-designers-in-the-middle-east-ad50-2022-revealed') }}"><h3>The Best Designers In The Middle East – AD50 2022 Revealed</h3></a>
                                        <p>Memory, tradition and modernity are the cornerstones of this Dubai and Syria-based studio making its mark with playful and exuberant objects that offer a refreshing twist on heritage. Unexpected materials, bright colours and a sense of humour hide the inherent complexity of their designs which are born from collaborations...</p>
                                        <a class="view-variant" href="{{ url('news/the-best-designers-in-the-middle-east-ad50-2022-revealed') }}">Read more</a>
                                    </div>
                                </div>
                                <div class="row mb-5">
                                    <div class="col-md-4">
                                        <a href="{{ url('news/architectural-digest') }}"><img src="{{ asset('public/img/articles/news-6-thumb.jpg') }}" class="mb-3" width="100%"></a>
                                    </div>
                                    <div class="col-md-8">
                                        <a href="{{ url('news/architectural-digest') }}"><h3>Architectural Digest 50</h3></a>
                                        <p></p>
                                        <a class="view-variant" href="{{ url('news/architectural-digest') }}">Read more</a>
                                    </div>
                                </div>
                                <div class="row mb-5">
                                    <div class="col-md-4">
                                        <a href="{{ url('news/reappropriating-the-form-are-studio-questions-time-with-new-collection') }}"><img src="{{ asset('public/img/articles/jdeed-thumb.jpg') }}" class="mb-3" width="100%"></a>
                                    </div>
                                    <div class="col-md-8">
                                        <a href="{{ url('news/reappropriating-the-form-are-studio-questions-time-with-new-collection') }}"><h3>Reappropriating The Form: ARE Studio Questions Time with New Collection</h3></a>
                                        <p>Bridging the past, present, and future, ARE Studio releases a new collection of furniture pieces that interrogate the processes of tradition meeting modernity. Marked by glitches and intentional abstractions, their chairs, stools, tables, and mirrors are meant to transform spaces. ARE’s self-proclaimed playground is the ...</p>
                                        <a class="view-variant" href="{{ url('news/reappropriating-the-form-are-studio-questions-time-with-new-collection') }}">Read more</a>
                                    </div>
                                </div>
                                <div class="row mb-5">
                                    <div class="col-md-4">
                                        <a href="{{ url('news/adloves-downtown-design-are-contemporary-design') }}"><img src="{{ asset('public/img/articles/adlove--thumb.jpg') }}" class="mb-3" width="100%"></a>
                                    </div>
                                    <div class="col-md-8">
                                        <a href="{{ url('news/adloves-downtown-design-are-contemporary-design') }}"><h3>#ADLoves ARE’s Bold And Bright Take On Tradition</h3></a>
                                        <p>Memory, tradition and modernity are the cornerstones of ARE, a studio making its mark with playful and exuberant pieces that offer a refreshing twist on heritage. Unexpected materials, bright colours and a sense of humour hide the inherent complexity of their designs that are ...</p>
                                        <a class="view-variant" href="{{ url('news/adloves-downtown-design-are-contemporary-design') }}">Read more</a>
                                    </div>
                                </div>
                                <div class="row mb-5">
                                    <div class="col-md-4">
                                        <a href="{{ url('news/al-beit-magazine') }}"><img src="{{ asset('public/img/articles/albeit-thumb.jpg') }}" width="100%" class="mb-3"></a>
                                    </div>
                                    <div class="col-md-8">
                                        <a href="{{ url('news/al-beit-magazine') }}"><h3>Al Beit Magazine</h3></a>
                                        <a class="view-variant" href="{{ url('news/al-beit-magazine') }}">Read more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
@endsection
