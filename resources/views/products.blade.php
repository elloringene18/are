@extends('master')

@section('css')
    <link rel="stylesheet" href="{{ asset('public/') }}/css/shop.css">
    <style>
        #top {
            background-image: url('{{ asset('public') }}/img/about-bg.png');
        }
    </style>
@endsection

@section('content')

    <div class="clearfix">
        <div class="container" style="position: relative;">
            <h1 class="top-title">OUR<br/>PRODUCTS</h1>
            <form action="{{ url('shop/') }}" method="get" id="filterForm">
                <div class="row">
                    <div class="col-md-3">
                        <div class="custom-select">
                            <select class="form-control" name="sort">
                                <option value>Sort By</option>
                                <option value="lprice" {{ isset($_GET['sort']) ? ( $_GET['sort'] == 'lprice' ? 'selected' : false ) : '' }}>Lowest Price First</option>
                                <option value="hprice" {{ isset($_GET['sort']) ? ( $_GET['sort'] == 'hprice' ? 'selected' : false ) : '' }}>Highest Price First</option>
                                <option value="aname" {{ isset($_GET['sort']) ? ( $_GET['sort'] == 'aname' ? 'selected' : false ) : '' }}>Name A-Z</option>
                                <option value="dname" {{ isset($_GET['sort']) ? ( $_GET['sort'] == 'dname' ? 'selected' : false ) : '' }}>Name Z-A</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="custom-select">
                            <select name="cat" class="form-control">
                                @foreach ($categories as $category)
                                    <option @if(isset($_GET['cat'])&& $_GET['cat']== $category->id) selected @endif value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" value="Filter" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row">
                @include('partials.product-list')
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection
