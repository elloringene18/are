
@extends('master')

@section('css')
    <link href="{{ asset('public/') }}/css/account.css" rel="stylesheet">
@endsection

@section('content')
    <section id="page" class="inner-page">
        <div class="container">
            <div class="row relative">
                <div class="col-md-12 pb-4 pt-4">
                    <h3 class="mb-4">MY ACCOUNT</h3>
                    <div class="row">
                        <div class="col-md-3">
                            <ul>
                                <li><a href="#" class="active">Orders</a></li>
                                {{--<li><a href="#">Delivery Address</a></li>--}}
                                {{--<li><a href="#">Profile</a></li>--}}
                                <li><a href="{{ url('logout') }}">Logout</a></li>
                            </ul>
                        </div>
                        <div class="col-md-9 border-left pl-5">

                            <span class="items-in-cart"><strong><span class="price">{{ count($data) }}</span> Orders Made</strong></span>
                            <table width="100%" class="mt-4">
                                <th>
                                    <tr>
                                        <td>Order ID</td>
                                        <td>Total</td>
                                        <td>Status</td>
                                        <td>Date</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4"><hr/></td>
                                    </tr>
                                </th>
                                @foreach($data as $item)
                                    <tr>
                                        <td  class="pb-2 pt-2">
                                            <a href="{{ url('account/order/'.$item->id) }}">
                                                <h5 class="product-name">#000{{ $item->id }}</h5>
                                            </a>
                                        </td>
                                        <td  class="pb-2 pt-2">
                                            {{ $item->total }} AED
                                        </td>
                                        <td  class="pb-2 pt-2 "><span class="price">{{ $item->status }}</span></td>
                                        <td  class="pb-2 pt-2 "><span class="price">{{ $item->created_at->format('d-m-Y') }}</span></td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js')
@endsection







