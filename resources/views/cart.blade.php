@extends('master')

@section('css')
    <link href="{{ asset('public') }}/css/cart.css" rel="stylesheet">
@endsection

@section('content')

    <section id="content" class="inner-page">
        <div class="container">
            <div class="row tc">
                <div class="col-md-9 col-sm-6">
                    <h3 class="mb-4">CART</h3>
                </div>
                <div class="col-md-3 col-sm-6">
                    <a href="{{ url('/shop') }}" class="float-left txt-dgold w-arrow-right">Continue shopping</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-9">
                    <?php $total = 0; ?>

                    @if(count(Auth::user()->cart))
                    <table width="100%" class="mb-3">
                        <th>
                            <tr class="text-uppercase txt-dgold pb-5">
                                <td>Item</td>
                                <td>Attributes</td>
                                <td>QTY</td>
                            </tr>
                        </th>
                            @foreach(Auth::user()->cart as $item)
                                <?php
                                    if($item->product_variant_id)
                                        $total += $item->variant->price * $item->qty;
                                    else
                                        $total += $item->product->price * $item->qty;
                                ?>
                                <tr>
                                    <td colspan="3"><hr class="mt-4 mb-4"/></td>
                                </tr>
                                <tr>
                                    <td class="first-col">
                                        <div class="product text-uppercase">
                                            <a href="{{ url('/product/'.$item->product->slug) }}">
                                                <div class="photo"><img src="{{ $item->product->photoUrl }}" width="100"></div>
                                                <div class="details">
                                                    <h6 class="name mb-1 mt-4">{{ $item->product->title }}</h6>
                                                    <p class="price txt-gold mb-1">
                                                        {{ $item->product_variant_id ? ''.$item->variant->price.' AED' : ''.$item->product->price.' AED' }}
                                                        @if($item->qty > 1)
                                                            X {{$item->qty}}
                                                             ( ${{ $item->product_variant_id ? $item->variant->price * $item->qty : $item->product->price * $item->qty }} )
                                                        @endif
                                                    </p>
                                                </div>
                                            </a>
                                        </div>
                                    </td>
                                    <td class="second-col">
                                        @if($item->product_variant_id)
                                            {{ $item->variant->key }}: {{ $item->variant->value }}
                                        @endif
                                    </td>
                                    <td class="third-col">
                                        <div class="qty-panel row">
                                            <div class="col-md-12">
                                                <a href="{{ url('/cart/reduce/'.$item->id) }}" class="less">-</a>
                                                <span class="qty">{{ $item->qty }}</span>
                                                <a href="{{ url('/cart/increase/'.$item->id) }}" class="more">+</a>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 mt-1">
                                                <a href="{{url('/cart/delete/'.$item->id)}}" class="txt-dgold">Remove</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                    </table>

                    @else
                        <p>Your cart is empty.</p>
                    @endif
                </div>
                <div class="col-md-3 ">

                    <div class="row">
                        <div class="col-md-12">
                    @if(count(Auth::user()->cart))
                        <div class="total">
                            <div class="left">Total</div>
                            <div class="right">{{$total}} AED</div>
                        </div>
                        <a href="{{ url('/cart/checkout/') }}">
                            <div class="checkout">
                                Proceed to checkout
                            </div>
                        </a>

                    @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p>&nbsp;</p>
                            <strong style="margin-top: 20px;">Delivery fee is not included.</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@section('js')
@endsection

