@extends('admin.partials.master')

@section('content')
<!-- partial -->
<div class="main-panel">
    <div class="content-wrapper">

        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h5>Orders</h5>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">

                        <div class="table-responsive">
                            <table class="table" id="dataTable">
                                <tr>
                                    <th onclick="sortTable(0)">Order #</th>
                                    <th onclick="sortTable(1)">Customer</th>
                                    <th onclick="sortTable(2)">Deliver Address</th>
                                    <th onclick="sortTable(2)">Total</th>
                                    <th onclick="sortTable(2)">Status</th>
                                    <th onclick="sortTable(2)">Date</th>
                                    <th>Action</th>
                                </tr>
                                @foreach($data as $item)
                                <tr>
                                    <td><a href="{{ url('admin/orders/'.$item->id) }}">#{{ $item->id }}</a></td>
                                    <td>{{ $item->user->name }}</td>
                                    <td>{{ $item->delivery_address }}</td>
                                    <td>{{ $item->total }} AED</td>
                                    <td>
                                        <select class="form-control orderStatus" data-id="{{$item->id}}" style="width:100px">
                                            <option value="unpaid" {{ $item->status == 'unpaid' ? 'selected' : '' }}>Unpaid</option>
                                            <option value="paid" {{ $item->status == 'paid' ? 'selected' : '' }}>Paid</option>
                                            <option value="shipped" {{ $item->status == 'shipped' ? 'selected' : '' }}>Shipped</option>
                                            <option value="delivered" {{ $item->status == 'delivered' ? 'selected' : '' }}>Delivered</option>
                                        </select>
                                    </td>
                                    <td>{{ $item->created_at->format('d-m-Y') }}</td>
                                    <td>
                                        <a href="{{ URL('admin/products/'.$item->id) }}">Edit</a>
                                        |
                                        <a href="{{ URL('admin/products/delete/'.$item->id) }}" onclick="return confirm('Are you sure you want to delete this item?');">Delete</a></td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 grid-margin stretch-card pagi">
                <div class="card">
                    <div class="card-body">
                        {{ $data->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script>
        $('.orderStatus').on('change',function(){
            status = $(this).val();
            orderId = $(this).attr('data-id');

            window.location = baseUrl+'/admin/orders/update-status/'+orderId+'/'+status;
        });
    </script>
@endsection
