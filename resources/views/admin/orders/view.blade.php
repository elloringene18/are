@extends('admin.partials.master')

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Orders</h5>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4 border-left">

                                    <p><h3><strong>Order :</strong> #{{$order->id}}</h3></p>
                                    <p><strong>Delivery Address:</strong> {{ $order->delivery_address }}</p>
                                    <p><strong>Total:</strong> {{ $order->total }}</p>
                                    <p><strong>Shipping:</strong> {{ $order->shipping_fee > 0 ? $order->shipping_fee.' AED' : 'FREE' }}</p>

                                    <p><strong>Status:</strong></p>
                                    <select class="form-control" id="orderStatus">
                                        <option value="unpaid" {{ $order->status == 'unpaid' ? 'selected' : '' }}>Unpaid</option>
                                        <option value="paid" {{ $order->status == 'paid' ? 'selected' : '' }}>Paid</option>
                                        <option value="shipped" {{ $order->status == 'shipped' ? 'selected' : '' }}>Shipped</option>
                                        <option value="delivered" {{ $order->status == 'delivered' ? 'selected' : '' }}>Delivered</option>
                                    </select>

                                    @if(Session::has('success'))
                                        <div class="row mt-3">
                                            <div class="col-md-12">
                                                <div class="alert alert-success">{{ Session::get('success') }}</div>
                                            </div>
                                        </div>
                                    @endif

                                    @if(Session::has('error'))
                                        <div class="row mt-3">
                                            <div class="col-md-12">
                                                <div class="alert alert-danger">{{ Session::get('error') }}</div>
                                            </div>
                                        </div>
                                    @endif

                                    <hr/>
                                    <span class="items-in-cart"><strong><span class="price">{{ count($order->items) }}</span> items</strong> in this order.</span>
                                </div>
                                <div class="col-md-8">
                                    <table width="100%" class="mt-4">
                                        <th>
                                            <tr>
                                                <td>Product</td>
                                                <td></td>
                                                <td>Qty</td>
                                                <td>Variant</td>
                                                <td>Total</td>
                                            </tr>
                                            <tr>
                                                <td colspan="4"><hr/></td>
                                            </tr>
                                        </th>
                                        @foreach($order->items as $item)
                                            <tr>
                                                <td  class="pb-2 pt-2"><img class="prodimg" src="{{ asset('public/'.$item->product->photo) }}" width="80"/></td>
                                                <td  class="pb-2 pt-2">
                                                    <a href="{{ url('product/'.$item->product->slug) }}">
                                                        <h5 class="product-name">{{ $item->product->title }}</h5>
                                                    </a>
                                                    <a href="{{ url('shop/'.$item->product->category->slug) }}"><span class="product-category">{{ $item->product->category->name }}</span></a>
                                                </td>
                                                <td  class="pb-2 pt-2">
                                                    <span class="price"> {{ $item->qty }} </span>
                                                </td>
                                                <td>{{ $item->variant ? $item->variant->value : '' }}</td>
                                                <td  class="pb-2 pt-2 "><span class="price">{{ $item->variant ? $item->variant->price : $item->product->price }} AED</span></td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 grid-margin stretch-card pagi">
                    <div class="card">
                        <div class="card-body">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script>
        $('#orderStatus').on('change',function(){
            status = $(this).val();
            orderId = '{{ $order->id }}';

            window.location = baseUrl+'/admin/orders/update-status/'+orderId+'/'+status;
        });
    </script>
@endsection
