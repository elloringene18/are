@extends('admin.partials.master')

@section('css')
    <style>
        .deleteVal {
            display: block;
            position: absolute;
            left: -20px;
            top: -10px;
            width: 20px;
            height: 20px;
            background-color: #ff7b7b;
            border-radius: 50%;
            text-align: center;
            font-size: 14px;
            line-height: 20px;
        }

        .values .value {
            background-color: #e6e6e6;
            padding: 10px;
            margin-bottom: 3px;
        }
    </style>
@endsection

@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Edit Product</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/products/update') }}" method="post" enctype="multipart/form-data">
                @csrf
                <input type="hidden" value="{{ $item->id }}" name="id">

                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Title</h4>
                                <div class="form-group">
                                    <input value="{{ $item->title }}" type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="title" >
                                </div>

                                <div class="form-group content">
                                    <label for="exampleInputEmail3">Description</label>
                                    <input type="hidden" name="description" value="{{ $item->description }}"/>
                                    <div class="summernote">
                                        {!! $item->description !!}
                                    </div>
                                </div>

                                <div class="form-group content">
                                    <label for="exampleInputEmail3">Price</label>
                                    <input value="{{ $item->price}}" type="number" class="form-control" id="exampleInputNamea1" placeholder="Price" name="price" >
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Variants</label>
                                    </div>
                                    <div class="col-md-6">
                                        {{--<a href="#" style="float:right;font-size:12px;" class="add-variant">+ Add a variant</a>--}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <hr/>
                                    </div>
                                </div>
                                <div id="variants">
                                    @if($variants)
                                        @foreach($variants as $variant)
                                            <div class="variant" id="variant-0">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label>Name (Example: Size, Weight, Volume, Color)</label>
                                                            <input type="text" class="form-control" name="key" placeholder="Size" value="{{$variant['key']}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="values">
                                                    @foreach($variant['values'] as $value)
                                                        <div class="row value">
                                                            <div class="col-md-6">
                                                                <a href="{{ url('admin/products/delete/value/'.$value['id']) }}" class="deleteVal">X</a>
                                                                <div class="form-group">
                                                                    <label>Value:</label>
                                                                    <input type="text" class="form-control" name="values[{{$value['id']}}][value]" value="{{$value['value']}}">
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label>Price:</label>
                                                                    <input type="text" class="form-control" name="values[{{$value['id']}}][price]" placeholder="100" value="{{$value['price']}}">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="variant" id="variant-0">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                        <label>Name (Example: Size, Weight, Volume, Color)</label>
                                                        <input type="text" class="form-control" name="key" placeholder="Size" value="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="values">
                                                <div class="row value">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Value:</label>
                                                            <input type="text" class="form-control" name="newvariants[0][value]" placeholder="Large" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <label>Price:</label>
                                                            <input type="text" class="form-control" name="newvariants[0][price]" placeholder="100" value="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a href="#" class="add-value" data-variant="0" style="font-size:12px;float:right;">+ Add Value</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Primary Image</label>
                                    <br/>
                                    @if($item->photo)
                                        <img src="{{ asset('public/'.$item->photo) }}" width="200">
                                        <br/>
                                        <br/>
                                    @endif
                                    <input type="file" class="form-control" name="photo">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Gallery Images</label>
                                    <div class="row">
                                        @foreach($item->gallery as $photo)
                                            <div class="col-md-3"><img src="{{ $photo['url'] }}" width="200"><br/><a href="{{ url('/admin/products/delete-photo/'.$photo['id'])}}">Delete</a> </div>
                                        @endforeach
                                    </div>
                                    <hr/>
                                    <input type="file" class="form-control" name="photos[]" multiple>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="form-control type_select" name="category_id" data-lang="en" id="category_id">
                                        @foreach($categories as $category)
                                            <option value="{{ $category['id'] }}" {{ $category['id'] == $item->category->id ? 'selected' : '' }}>{{ $category->parent ? '--- ' : '' }}{{ $category['name'] }}</option>
                                        @endforeach
                                    </select>
                                    <div class="row" id="attributes">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Brand</label>
                                    <select class="form-control type_select" name="seller_id">
                                        @foreach($sellers as $seller)
                                            <option {{-- $seller->id == $item->seller->id ? 'selected' : '' --}} value="{{ $seller->id }}">{{ $seller->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>For Request</label>
                                    <select class="form-control type_select" name="for_request">
                                        <option value="0" {{ $item->for_request == 0 ? 'selected' : '' }}>No</option>
                                        <option value="1" {{ $item->for_request == 1 ? 'selected' : '' }}>Yes</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('public/admin/js/file-upload.js') }}"></script>
    <script>
        var currentAttributes = JSON.parse("<?php print_r($currentAttributes); ?>");

        function getCategoryAttributes(){
            $('#attributes').html('');
            var id = $('#category_id').val();
            $.ajax({
                type: "GET",
                url: baseUrl+'/api/categories/get-attributes/'+id,
                success: function(response){
                    console.log(response);

                    $.each( response, function( key, cat ) {
                        label = '<div class="col-md-4"><br/>';
                        label += cat.name+':<br/><br/>';

                        $.each( cat.values, function( key, value ) {
                            label += '<label><input type="checkbox" name="attributes[]" '+ (currentAttributes.includes(value.id) ? 'checked' : '') +' value="'+value.id+'"/> '+value.value+'</label><br/>';
                            console.log(value.value)
                        });

                        label += "</div>";
                        $('#attributes').prepend(label);
                    });
                },
                statusCode: {
                    // 401: function() {
                    //     window.location.href = baseUrl+'/login'; //or what ever is your login URI
                    // }
                },
                complete : function (event,error){
                    // hideLoader();
                }
            });
        }
        getCategoryAttributes();

        $('#category_id').on('change',function(){

            getCategoryAttributes();
        });


        function addValue(variantId){
            randomVarId = getRandomArbitrary();
            randomValId = getRandomArbitrary();
            item = '';

            item = '\n' +
                '                                            <div class="values">\n' +
                '                                                <div class="row value">\n' +
                '                                                    <div class="col-md-6">\n' +
                '                                                        <div class="form-group">\n' +
                '                                                            <label>Value:</label>\n' +
                '                                                            <input type="text" class="form-control" name="newvariants['+randomValId+'][value]" placeholder="Large" value="">\n' +
                '                                                        </div>\n' +
                '                                                    </div>\n' +
                '                                                    <div class="col-md-6">\n' +
                '                                                        <div class="form-group">\n' +
                '                                                            <label>Price:</label>\n' +
                '                                                            <input type="text" class="form-control" name="newvariants['+randomValId+'][price]" placeholder="100" value="">\n' +
                '                                                        </div>\n' +
                '                                                    </div>\n' +
                '                                                </div>\n' +
                '                                        </div>';

            $('#variant-'+variantId).append(item);
        }

        function addValueClickEvent(){
            $('.add-value').each(function(){
                variantId = $(this).attr('data-variant');
                $(this).on('click',function(e){
                    e.preventDefault();
                    addValue(variantId);
                });
            });
        }
        addValueClickEvent();

        function getRandomArbitrary() {
            return Math.floor(Math.random() * (999999 - 1) + 1);
        }
    </script>
@endsection
