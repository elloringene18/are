@extends('admin.partials.master')

@section('css')
    <style>
        .modalcolor {
            width: 20px;
            height: 20px;
            margin-right: 5px;
            margin-bottom: -4px;
            display: inline-block;
            -webkit-border-radius: 50%;
            -moz-border-radius: 50%;
            border-radius: 50%;
            margin-left: 5px;
        }
    </style>
@endsection
@section('content')
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">

            <div class="row">
                <div class="col-md-12 grid-margin stretch-card">
                    <div class="card">
                        <div class="card-body">
                            <h5>Add a Product</h5>
                        </div>
                    </div>
                </div>
            </div>

            <form class="forms-sample" action="{{ url('admin/products/store') }}" method="post" enctype="multipart/form-data">
                <input type="hidden" value="{!! csrf_token() !!}" name="_token">

                @if(Session::has('success'))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="alert-success alert">{{ Session::get('success') }}</div>
                        </div>
                    </div>
                @endif
                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Name</h4>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="exampleInputNamea1" placeholder="Title" name="title" required>
                                </div>

                                <div class="form-group content">
                                    <label for="exampleInputEmail3">Description</label>
                                    <input type="hidden" name="description" value=""/>
                                    <div class="summernote">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Variants</label>
                                    </div>
                                    <div class="col-md-6">
                                        {{--<a href="#" style="float:right;font-size:12px;" class="add-variant">+ Add a variant</a>--}}
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <hr/>
                                    </div>
                                </div>
                                <div id="variants">
                                    <div class="variant" id="variant-0">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Name (Example: Size, Weight, Volume, Color)</label>
                                                    <input type="text" class="form-control" name="variants[0][name]" placeholder="Size" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="values">
                                            <div class="row value">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Value:</label>
                                                        <input type="text" class="form-control" name="variants[0][values][0][value]" placeholder="Large" value="">
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>Price:</label>
                                                        <input type="text" class="form-control" name="variants[0][values][0][price]" placeholder="100" value="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <a href="#" class="add-value" data-variant="0" style="font-size:12px;float:right;">+ Add Value</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Primary Image</label>
                                    <input type="file" class="form-control" name="image" data-lang="en">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Gallery Images</label>
                                    <input type="file" class="form-control" name="photos[]" multiple>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Category</label>
                                    <select class="form-control type_select" name="category_id" data-lang="en" id="category_id">
                                        @foreach($categories as $category)
                                            <option value="{{ $category['id'] }}">{{ $category->parent ? '--- ' : '' }}{{ $category['name'] }}</option>
                                        @endforeach
                                    </select>
                                    <div class="row" id="attributes">

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label>Price</label>
                                    <input type="number" class="form-control" name="price" value="0">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Brand</label>
                                    <select class="form-control type_select" name="seller_id">
                                        @foreach($sellers as $seller)
                                            <option {{ isset($_GET['brand_id']) ? ($_GET['brand_id'] == $seller->id ? 'selected' : false) :  '' }} value="{{ $seller->id }}">{{ $seller->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>For Request</label>
                                    <select class="form-control type_select" name="for_request">
                                        <option value="0">No</option>
                                        <option value="1">Yes</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 grid-margin stretch-card">
                        <div class="card">
                            <div class="card-body">
                                <button type="submit" class="btn btn-success mr-2">Submit</button>
                                <button class="btn btn-light">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('js')
    <script src="{{ asset('public/admin/js/file-upload.js') }}"></script>
    <script>

        function getCategoryAttributes(){
            $('#attributes').html('');
            var id = $('#category_id').val();
            $.ajax({
                type: "GET",
                url: baseUrl+'/api/categories/get-attributes/'+id,
                success: function(response){
                    console.log(response);

                    $.each( response, function( key, cat ) {
                        console.log(cat.name);
                        label = '<div class="col-md-4"><br/>';
                        label += cat.name+':<br/><br/>';

                        $.each( cat.values, function( key, value ) {
                            if(cat.name=="Color")
                                label += '<label><input type="checkbox" name="attributes[]" value="'+value.id+'"/><span class="modalcolor" style="background-color:'+value.value+'"></span></label><br/>';
                            else
                                label += '<label><input type="checkbox" name="attributes[]" value="'+value.id+'"/> '+value.value+'</label><br/>';
                        });

                        label += "</div>";
                        $('#attributes').prepend(label);
                    });
                },
                statusCode: {
                    // 401: function() {
                    //     window.location.href = baseUrl+'/login'; //or what ever is your login URI
                    // }
                },
                complete : function (event,error){
                    // hideLoader();
                }
            });
        }
        getCategoryAttributes();

        $('#category_id').on('change',function(){

            getCategoryAttributes();
        });

        $('.add-variant').on('click',function(e){
            e.preventDefault();
        });

        function addValue(variantId){
            randomVarId = getRandomArbitrary();
            randomValId = getRandomArbitrary();
            item = '                                            <div class="row value">'+
                '                                                <div class="col-md-6">'+
                '                                                    <div class="form-group">'+
                '                                                        <label>Value:</label>'+
                '                                                        <input type="text" class="form-control" name="variants[0][values]['+randomValId+'][value]" placeholder="Large" value="">'+
                '                                                    </div>'+
                '                                                </div>'+
                '                                                <div class="col-md-6">'+
                '                                                    <div class="form-group">'+
                '                                                        <label>Price:</label>'+
                '                                                        <input type="text" class="form-control" name="variants[0][values]['+randomValId+'][price]" placeholder="100" value="">'+
                '                                                    </div>'+
                '                                                </div>'+
                '                                            </div>\n';

            $('#variant-'+variantId).append(item);
        }

        function addValueClickEvent(){
            $('.add-value').each(function(){
                variantId = $(this).attr('data-variant');
                $(this).on('click',function(e){
                    e.preventDefault();
                    addValue(variantId);
                });
            });
        }
        addValueClickEvent();

        function getRandomArbitrary() {
            return Math.floor(Math.random() * (999999 - 1) + 1);
        }
    </script>
@endsection
