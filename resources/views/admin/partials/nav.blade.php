<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{ URL('admin/orders') }}">
                <i class="menu-icon mdi mdi-format-list-bulleted"></i>
                <span class="menu-title">Orders</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#products" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-book"></i>
                <span class="menu-title">Products</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="products">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/products') }}">View Products</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/products/create/') }}">Add a Product</a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#categories" aria-expanded="true" aria-controls="homepage">
                <i class="menu-icon mdi mdi-book"></i>
                <span class="menu-title">Categories</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse in" id="categories">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/categories') }}">View Categories</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ URL('admin/categories/create/') }}">Add a Category</a>
                    </li>
                </ul>
            </div>
        </li>

       

        {{--<li class="nav-item">--}}
            {{--<a class="nav-link" data-toggle="collapse" href="#sellers" aria-expanded="true" aria-controls="homepage">--}}
                {{--<i class="menu-icon mdi mdi-account-circle"></i>--}}
                {{--<span class="menu-title">Brands</span>--}}
                {{--<i class="menu-arrow"></i>--}}
            {{--</a>--}}
            {{--<div class="collapse in" id="sellers">--}}
                {{--<ul class="nav flex-column sub-menu">--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/brands') }}">View Brands</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/brands/create/') }}">Add a Brand</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</li>--}}
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link" data-toggle="collapse" href="#categories" aria-expanded="true" aria-controls="homepage">--}}
                {{--<i class="menu-icon mdi mdi-information"></i>--}}
                {{--<span class="menu-title">Categories</span>--}}
                {{--<i class="menu-arrow"></i>--}}
            {{--</a>--}}
            {{--<div class="collapse in" id="categories">--}}
                {{--<ul class="nav flex-column sub-menu">--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/categories') }}">View Categories</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/categories/create/') }}">Add a Category</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</li>--}}
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link" data-toggle="collapse" href="#administrators" aria-expanded="true" aria-controls="homepage">--}}
                {{--<i class="menu-icon mdi mdi-content-copy"></i>--}}
                {{--<span class="menu-title">About Page</span>--}}
                {{--<i class="menu-arrow"></i>--}}
            {{--</a>--}}
            {{--<div class="collapse in" id="administrators">--}}
                {{--<ul class="nav flex-column sub-menu">--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/administrators') }}">View Team Members</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/administrators/create/') }}">Add a Team Member</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/partners') }}">View Partners</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/partners/create/') }}">Add a Partner</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/page-contents/about/') }}">Other Page Contents</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</li>--}}
        <li class="nav-item">
            <a class="nav-link" href="{{ URL('admin/subscribers') }}">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Subscribers</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ URL('admin/contacts') }}">
                <i class="menu-icon mdi mdi-content-copy"></i>
                <span class="menu-title">Inquiries</span>
                <i class="menu-arrow"></i>
            </a>
        </li>
        {{--<li class="nav-item">--}}
            {{--<a class="nav-link" data-toggle="collapse" href="#settings" aria-expanded="true" aria-controls="homepage">--}}
                {{--<i class="menu-icon mdi mdi-content-copy"></i>--}}
                {{--<span class="menu-title">Settings</span>--}}
                {{--<i class="menu-arrow"></i>--}}
            {{--</a>--}}
            {{--<div class="collapse in" id="settings">--}}
                {{--<ul class="nav flex-column sub-menu">--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/settings/home-items') }}">Home Categories</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/page-contents/header/') }}">Header Settings</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/page-contents/footer/') }}">Footer Settings</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{{ URL('admin/page-contents/contact/') }}">Contact Settings</a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</li>--}}
    </ul>
</nav>
