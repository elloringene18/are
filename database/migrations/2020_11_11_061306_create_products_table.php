<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            // ['name','slug','title','description','photo','price','category_id'];
            $table->string('slug')->unique();
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('photo')->nullable();
            $table->string('photo_full')->nullable();
            $table->string('year')->nullable();
            $table->float('price')->default(0);
            $table->smallInteger('for_sale')->default(0);
            $table->boolean('is_faved')->default(false);
            $table->tinyInteger('for_request')->default(0);
            $table->integer('stock')->default(0);
            $table->bigInteger('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
