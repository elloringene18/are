<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [ 'name' => 'Admin', 'slug' => 'admin' ],
            [ 'name' => 'Customer', 'slug' => 'customer' ],
        ];

        foreach ($data as $item)
        {
            $newRow = Role::create($item);
        }
    }
}
