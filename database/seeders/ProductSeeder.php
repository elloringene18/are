<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\Seller;
use App\Models\Upload;
use App\Services\Uploaders\ProductImagesUploader;
use App\Traits\CanCreateSlug;
use Faker\Generator;
use Illuminate\Console\Command;
use Illuminate\Database\Seeder;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Symfony\Component\Console\Output\ConsoleOutput;

class ProductSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Product $model,Generator $faker, Command $command, ProductImagesUploader $uploader)
    {
        $this->model = $model;
        $this->faker = $faker;
        $this->command = $command;
        $this->uploader = $uploader;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        $data = array(
            0 => array('title' => 'CTRL-P SEAT', 'price' => '6500', 'brand' => 'ARE', 'category' => 'Furniture', 'description' => 'Material: Walnut wood/Mother of Pearl<br/>Size: W96 x H115',
                'photo' => 'img/products/CTRL-P-SEAT.jpg',
                'for_request' => 1,
            ),
            1 => array('title' => '60 ° MIRROR', 'price' => '9000', 'brand' => 'ARE', 'category' => 'Furniture', 'description' => 'Material: Wood/Mother of Pearl/Fiber/Epoxy<br/>Size: W130cm x H280cm',
                'photo' => 'img/products/60-MIRROR.jpg',
                'for_request' => 1,
                'gallery' => [
                    'img/products/60-MIRROR-2.jpg',
                ]
            ),
            2 => array('title' => '144P SEAT', 'price' => '4700', 'brand' => 'ARE', 'category' => 'Furniture', 'description' => 'Material: Walnut wood<br/>Size: W48cm x H115cm',
                'photo' => 'img/products/144P-SEAT.jpg',
                'for_request' => 0,
                'gallery' => [
                    'img/products/144P-SEAT2.jpg',
                ]
            ),
            3 => array('title' => 'DISTORTED END TABLE', 'price' => '2500', 'brand' => 'ARE', 'category' => 'Furniture', 'description' => 'Material: Wood/Mother of Pearl<br/>Size: W52cm x H65cm',
                'photo' => 'img/products/DISTORTED-END-TABLE.jpg',
                'for_request' => 0,
                'gallery' => [
                    'img/products/DISTORTED-END-TABLE2.jpg',
                ]
            ),
            4 => array('title' => 'WHAT YOU SEE ISNT ALL THERE IS MIRROR', 'price' => '9500', 'brand' => 'ARE', 'category' => 'Furniture', 'description' => 'Material: Wood/Mother of Pearl/Neon<br/>Size: W130cm x H280cm',
                'photo' => 'img/products/WHAT-YOU-SEE-ISNT-ALL-THERE-IS-MIRROR.jpg',
                'for_request' => 1,
            ),
            5 => array('title' => 'RGB SEAT', 'price' => '4500', 'brand' => 'ARE', 'category' => 'Furniture', 'description' => 'Material: Walnut wood/Epoxy<br/>Size: W48cm x H115cm',
                'photo' => 'img/products/RGB-SEAT.jpg',
                'for_request' => 0,
                'gallery' => [
                    'img/products/RGB-SEAT2.jpg',
                ]
            ),
            6 => array('title' => 'CYMK END TABLE', 'price' => '2900', 'brand' => 'ARE', 'category' => 'Furniture', 'description' => 'Material: Wood/Mother of Pearl/Neon<br/>Size: W52cm x H65cm',
                'photo' => 'img/products/CYMK-END-TABLE.jpg',
                'for_request' => 0,
                'gallery' => [
                    'img/products/CYMK-END-TABLE2.jpg',
                ]
            ),
            7 => array('title' => 'LOVE MIRROR', 'price' => '9400', 'brand' => 'ARE', 'category' => 'Furniture', 'description' => 'Material: Wood/Mother of Pearl/Plexi Glass/Neon<br/>Size: W130cm x H280cm</br></br><strong style="font-size: 14px;">This product is customizable in terms of plexi glass color and neon color/word</strong>',
                'photo' => 'img/products/LOVE-MIRROR.jpg',
                'for_request' => 1,
                'gallery' => [
                    'img/products/LOVE-MIRROR2.jpg',
                ]
            ),
            8 => array('title' => 'SORTED PIXEL SEAT', 'price' => '7300', 'brand' => 'ARE', 'category' => 'Furniture', 'description' => 'Material: Wood/Mother of Pearl/Epoxy<br/>Size: W48cm x H115cm',
                'photo' => 'img/products/SORTED-PIXEL-SEAT.jpg',
                'for_request' => 1,
                'gallery' => [
                    'img/products/SORTED-PIXEL-SEAT2.jpg',
                ]
            ),
            9 => array('title' => 'INT-MODE 0 END TABLE', 'price' => '3700', 'brand' => 'ARE', 'category' => 'Furniture', 'description' => 'Material: Wood/Polyster<br/>Size: W52cm x H60cm',
                'photo' => 'img/products/INT-MODE-0-END-TABLE.jpg',
                'for_request' => 0,
                'gallery' => [
                    'img/products/INT-MODE-0-END-TABLE.jpg',
                    'img/products/INT-MODE-0-END-TABLE2.jpg',
                    'img/products/INT-MODE-0-END-TABLE3.jpg',
                    'img/products/INT-MODE-0-END-TABLE4.jpg',
                ]
            ),
            10 => array('title' => 'ANAGLYPH SEAT', 'price' => '4500', 'brand' => 'ARE', 'category' => 'Furniture', 'description' => 'Material: Wood/Epoxy<br/>Size: W48cm x H115cm',
                'photo' => 'img/products/ANAGLYPH-SEAT.jpg',
                'for_request' => 0,
                'gallery' => [
                    'img/products/ANAGLYPH-SEAT2.jpg',
                    'img/products/ANAGLYPH-SEAT3.jpg',
                ]
            ),
            11 => array('title' => 'SLC-D END TABLE', 'price' => '2500', 'brand' => 'ARE', 'category' => 'Furniture', 'description' => 'Material: Wood/Mother of Pearl<br/>Size: W52cm x H65cm',
                'photo' => 'img/products/SLC-D-END-TABLE.jpg',
                'for_request' => 0,
                'gallery' => [
                    'img/products/SLC-D-END-TABLE2.jpg',
                    'img/products/SLC-D-END-TABLE3.jpg',
                    'img/products/SLC-D-END-TABLE4.jpg',
                    'img/products/SLC-D-END-TABLE5.jpg',
                ]
            ),
            12 => array('title' => 'DATAMOSHING SEAT', 'price' => '2600', 'brand' => 'ARE', 'category' => 'Furniture', 'description' => 'Material: Wood<br/>Size:W48cm x H115cm',
                'photo' => 'img/products/DATAMOSHING-SEAT.jpg',
                'for_request' => 0,
                'gallery' => [
                    'img/products/DATAMOSHING-SEAT2.jpg',
                ]
            ),
            13 => array('title' => 'PURPLE SCREEN MIRROR', 'price' => '5300', 'brand' => 'ARE', 'category' => 'Furniture', 'description' => 'Material: Wood/Mother of Pearl/Epoxy<br/>Size: W130cm x H280cm',
                'photo' => 'img/products/PURPLE-SCREEN-MIRROR.jpg',
                'for_request' => 0,
                'gallery' => [
                    'img/products/PURPLE-SCREEN-MIRROR2.jpg',
                    'img/products/PURPLE-SCREEN-MIRROR3.jpg',
                    'img/products/PURPLE-SCREEN-MIRROR4.jpg',
                ]
            ),
            14 => array('title' => 'COLOR - CODE END TALE', 'price' => '2500', 'brand' => 'ARE', 'category' => 'Furniture', 'description' => 'Material: Wood/Mother of Pearl<br/>Size: W52cm x H65cm',
                'photo' => 'img/products/color-code-end-table.jpg',
                'for_request' => 0,
                'gallery' => [
                    'img/products/color-code-end-table2.jpg',
                    'img/products/color-code-end-table3.jpg',
                ]
            ),
            15 => array('title' => 'HOLOGRAPH MIRROR', 'price' => '6600', 'brand' => 'ARE', 'category' => 'Furniture', 'description' => 'Material: Wood/Mother of Pearl<br/>Size: W130cm x H280cm',
                'photo' => 'img/products/holograph-mirror.jpg',
                'for_request' => 1,
                'gallery' => [
                    'img/products/holograph-mirror2.jpg',
                    'img/products/holograph-mirror3.jpg',
                    'img/products/holograph-mirror4.jpg',
                ]
            ),
            16 => array('title' => 'HIGH RES TABLE', 'price' => '2500', 'brand' => 'ARE', 'category' => 'Furniture', 'description' => 'Material: Wood<br/>Size: W52cm x H65cm',
                'photo' => 'img/products/hi-res-table.jpg',
                'for_request' => 0,
                'gallery' => [
                    'img/products/hi-res-table2.jpg',
                    'img/products/hi-res-table3.jpg',
                ]
            ),
        );

        foreach($data as $item){
            $input = [];

            $slug = Str::slug($item['category']);

            $category = Category::where('slug',$slug)->first();

            if(!$category)
                $category = Category::create(['name'=>$item['category'],'slug'=>$slug]);

            $input = [
                'title' => $item['title'],
                'for_request' => $item['for_request'],
                'slug' => $this->generateSlug($item['title']),
                'photo' => null,
                'photo_full' => null,
                'category_id' => $category->id,
                'is_faved' => (rand(1,10) >= 5 ? 1 : 0),
                'description' => $item['description'],
                'price' => isset($item['price']) ? floatval($item['price']) : 0,
            ];

            if(isset($item['photo'])){
                $image =  new UploadedFile( public_path($item['photo']), 'tmp.jpg', 'image/jpeg',null,true);

                $img = Image::make($image);
                $img->resize(260, 260);

                $destinationPath = 'public/uploads/products';
                $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

                Image::make($image->getRealPath())->fit(360, 360)->save($destinationPath.'/'.$newFileName);
                $input['photo'] = 'uploads/products/'. $newFileName;

                $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();
                Image::make($image->getRealPath())->fit(680, 680)->save($destinationPath.'/'.$newFileName);
                $input['photo_full'] = 'uploads/products/'. $newFileName;
            }

            $newproduct = Product::create($input);

            if($newproduct){

                if(isset($item['variants'])){
                    foreach($item['variants'] as $variant){
                        $newproduct->variants()->create($variant);
                    }
                }

                $brandSlug = Str::slug($item['brand']);

                $seller = Seller::where('slug',$brandSlug)->first();

                if(!$seller)
                    $seller = Seller::create(['name'=>$item['brand'],'slug'=>$brandSlug,'bio'=>'']);

                $newproduct->seller()->sync($seller->id);

                if(isset($item['gallery'])){
                    foreach ($item['gallery'] as $photoUrl) {

                        Log::info(public_path($photoUrl));

                        $image =  new UploadedFile( public_path($photoUrl), 'tmp.jpg', 'image/jpeg',null,true);
                        $photo = ($image != null ? $this->uploader->upload($image) : false);
                        if ($photo)
                            $newproduct->uploads()->createMany($photo);
                    }
                }

            }
        }
    }

}
