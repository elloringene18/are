<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $users = [
            [
                'user' => [
                    'name' => 'Admin',
                    'email' => 'info@a--re.com',
                    'password' => \Illuminate\Support\Facades\Hash::make('a-r-e@admin!2021#'),
                ],
                'role' => 'admin'
            ],
        ];

        foreach ($users as $index=>$user)
        {
            $newUser = User::create($user['user']);
            $roleId = \App\Models\Role::where('slug',$user['role'])->pluck('id')->first();
            $newUser->role()->sync($roleId);
        }
    }
}
