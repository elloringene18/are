<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\CategoryParent;
use App\Traits\CanCreateSlug;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use SebastianBergmann\Environment\Console;
use Symfony\Component\Console\Output\ConsoleOutput;

class CategorySeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(Category $model)
    {
        $this->model = $model;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        $data = [
            [
                'name' => "Furniture",
//                'attributes' => [
//                    [ 'name' => 'Size',
//                        'values' => [
//                            [ 'value' => 'Small' ],
//                            [ 'value' => 'Medium' ],
//                            [ 'value' => 'Large' ],
//                        ]
//                    ],
//                    [ 'name' => 'Material',
//                        'values' => [
//                            [ 'value' => 'Cotton' ],
//                            [ 'value' => 'Fiber' ],
//                        ]
//                    ],
//                ]
            ],
//            [ 'name' => "Women's Clothing",
//                'attributes' => [
//                    [ 'name' => 'Size',
//                        'values' => [
//                            [ 'value' => 'Small' ],
//                            [ 'value' => 'Medium' ],
//                            [ 'value' => 'Large' ],
//                        ]
//                    ],
//                    [ 'name' => 'Material',
//                        'values' => [
//                            [ 'value' => 'Cotton' ],
//                            [ 'value' => 'Fiber' ],
//                        ]
//                    ],
//                    [ 'name' => 'Type',
//                        'values' => [
//                            [ 'value' => 'Sports wear' ],
//                            [ 'value' => 'Casual' ],
//                        ]
//                    ],
//                ]
//            ],
//            [
//                'name' => "Men's Shoes",
//                'attributes' => [
//                    [ 'name' => 'Size',
//                        'values' => [
//                            [ 'value' => '6' ],
//                            [ 'value' => '7' ],
//                            [ 'value' => '8' ],
//                            [ 'value' => '9' ],
//                            [ 'value' => '10' ],
//                        ]
//                    ],
//                    [ 'name' => 'Type',
//                        'values' => [
//                            [ 'value' => 'Sports wear' ],
//                            [ 'value' => 'Casual' ],
//                        ]
//                    ],
//                ]
//            ],
//            [
//                'name' => "Women's Shoes",
//                'attributes' => [
//                    [ 'name' => 'Size',
//                        'values' => [
//                            [ 'value' => '6' ],
//                            [ 'value' => '7' ],
//                            [ 'value' => '8' ],
//                            [ 'value' => '9' ],
//                            [ 'value' => '10' ],
//                        ]
//                    ],
//                    [ 'name' => 'Type',
//                        'values' => [
//                            [ 'value' => 'Sports wear' ],
//                            [ 'value' => 'Casual' ],
//                        ]
//                    ],
//                ]
//            ],
        ];

        foreach ($data as $item)
        {
            $category = [];
            $category['name'] = $item['name'];
            $category['slug'] = $this->generateSlug($item['name']);
//            $category['icon'] = $item['icon'];
//            $category['banner_square'] = $item['banner_square'];
//            $category['banner_long'] = $item['banner_long'];

            $category = Category::create($category);

//            foreach ($item['attributes'] as $attr){
//
//                $attribute = [];
//                $attribute['name'] = $attr['name'];
//                $attribute['slug'] = \Illuminate\Support\Str::slug($attr['name']);
//
//                $newAttribute = $category->attributes()->create($attribute);
//
//                foreach ($attr['values'] as $value){
//                    $newAttribute->values()->create([
//                        'slug' => Str::slug($value['value']),
//                        'value' => $value['value']
//                    ]);
//                }
//            }
//
//            if($category && isset($item['children'])){
//                foreach ($item['children'] as $sub){
//
//                    $subcat = [];
//                    $subcat['name'] = $sub['name'];
//                    $subcat['slug'] = \Illuminate\Support\Str::slug($sub['name']);
//
//                    $newSub = Category::create($subcat);
//
//                    CategoryParent::create([
//                        'parent_category_id' => $category->id,
//                        'category_id' => $newSub->id,
//                    ]);
//
//                    foreach ($sub['attributes'] as $attr){
//
//                        $attribute = [];
//                        $attribute['name'] = $attr['name'];
//                        $attribute['slug'] = \Illuminate\Support\Str::slug($attr['name']);
//
//                        $newAttribute = $newSub->attributes()->create($attribute);
//
//                        foreach ($attr['values'] as $value){
//                            $newAttribute->values()->create([
//                                'slug' => Str::slug($value['value']),
//                                'value' => $value['value']
//                            ]);
//                        }
//                    }
//                }
//            }
        }
    }
}

