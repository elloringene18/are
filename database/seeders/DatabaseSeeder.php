<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call(UserTableSeeder::class);
        $this->call(CategorySeeder::class);
//        $this->call(CategoryAttributeSeeder::class);
        $this->call(SellerSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(RoleTableSeeder::class);
        $this->call(CountrySeeder::class);
//        $this->call(CitySeeder::class);
    }
}
