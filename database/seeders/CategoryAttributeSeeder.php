<?php

namespace Database\Seeders;

use App\Models\Category;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Symfony\Component\Console\Output\ConsoleOutput;

class CategoryAttributeSeeder extends Seeder
{

    public function __construct(Generator $faker)
    {
        $this->faker = $faker;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();
        $categories = Category::get();

        foreach ($categories as $category)
        {
            if($category->children){
                foreach ($category->attributes as $attribute){

                    if($attribute->slug=='color'){
                        for($x=0;$x<10;$x++){
                            $word = $this->faker->hexColor;
                            $attribute->values()->create([
                                'slug'=> $word,
                                'value'=> $word,
                            ]);
                        }
                    } else {
                        for($x=0;$x<rand(5,10);$x++){
                            $word = $this->faker->word;
                            $attribute->values()->create([
                                'slug'=> Str::slug($word),
                                'value'=> $word,
                            ]);
                        }
                    }

                }
            }
        }
    }
}
