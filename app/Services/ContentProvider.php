<?php

namespace App\Services;

use App\Models\Administrator;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\HomeCategory;
use App\Models\MonthArtist;
use App\Models\PageContent;
use App\Models\Product;
use App\Models\Sponsor;
use Carbon\Carbon;

class ContentProvider {

    public function getAdministrators($lang){
        $data = Administrator::get();

        $results = [];

        foreach ($data as $id => $cat){

            if($lang=='en'){
                $results[$id]['name'] = $cat->name;
                $results[$id]['title'] = $cat->position;
            } else {
                $results[$id]['name'] = $cat->name_ar;
                $results[$id]['title'] = $cat->position_ar;
            }

            $results[$id]['photo'] = $cat->photoUrl;
        }

        return $results;
    }

    public function getSponsors($lang){
        $data = Sponsor::get();

        $results = [];

        foreach ($data as $id => $cat){

            if($lang=='en'){
                $results[$id]['name'] = $cat->name;
            } else {
                $results[$id]['name'] = $cat->name_ar;
            }

            $results[$id]['photo'] = $cat->photoUrl;
        }

        return $results;
    }

    public function getCats($length,$lang){
        $data = Category::whereDoesntHave('parent')->limit($length)->get();

        $results = [];

        foreach ($data as $id => $cat){

            if($lang=='en'){
                $results[$id]['name'] = $cat->name;
            } else {
                $results[$id]['name'] = $cat->name_ar;
            }

            $results[$id]['slug'] = $cat->slug;
            $results[$id]['children'] = [];

            foreach ($cat->children as $cid=>$child){

                if($lang=='en'){
                    $results[$id]['children'][$cid]['name'] = $child->name;
                } else {
                    $results[$id]['children'][$cid]['name'] = $child->name_ar;
                }

                $results[$id]['children'][$cid]['slug'] = $child->slug;
            }

        }

        return $results;
    }

    public function getHomeCategories($lang='en'){
        $results = HomeCategory::get();

        $data = [];

        foreach ($results as $id=>$cat){
            $data[$id]['name'] = $cat->category->name;

            if($lang=='ar')
                $data[$id]['name'] = $cat->category->name_ar;

            $data[$id]['slug'] = $cat->category->slug;
        }

        return $data;
    }

    public function getPageContent($page,$lang='en'){
        $results = PageContent::where('page',$page)->get();
        $data = [];

        foreach ($results as $result){
            if($lang=='en')
                $data[$result->section] = $result->content;
            else
                $data[$result->section] = $result->content_ar;
        }

        return $data;
    }

    public function getPageContentSingle($page,$section,$lang='en'){
        $results = PageContent::where($page);
        return $results;
    }

    public function getArtistOfTheMonth($date,$lang,$artworkLength = 4,$full = null){

        $results = MonthArtist::where('date_from','>=',Carbon::parse($date)->firstOfMonth())->where('date_to','<=',Carbon::parse($date)->lastOfMonth())->first();

        if(!$results)
            $results = MonthArtist::orderBy('date_from','DESC')->first();

        if($lang=='en'){
            $data['name'] = $results->artist->name;
            $data['bio'] = $results->artist->bio;
        } else {
            $data['name'] = $results->artist->name_ar;
            $data['bio'] = $results->artist->bio_ar;
        }

        $data['slug'] = $results->artist->slug;
        $data['artworks'] = [];
        $data['featured_artwork'] = Product::find($results->product_id)->photoUrl;
        $data['featured_artworks'] = $results->products;

        $artworks = $results->artist->products()->orderBy('id','DESC')->limit($artworkLength)->get();

        if($artworks)
            foreach($artworks as $id=>$product){

                if($lang=='en'){
                    $data['artworks'][$id]['title'] = $product->title;
                    $data['artworks'][$id]['description'] = $product->description;
                } else {
                    $data['artworks'][$id]['title'] = $product->title_ar;
                    $data['artworks'][$id]['description'] = $product->description_ar;
                }

                $data['artworks'][$id]['id'] = $product->id;
                $data['artworks'][$id]['slug'] = $product->slug;
                $data['artworks'][$id]['photo'] = $product->photo;
                $data['artworks'][$id]['photo_full'] = $product->photo_full;
                $data['artworks'][$id]['year'] = $product->year;
                $data['artworks'][$id]['price'] = $product->price;
                $data['artworks'][$id]['for_sale'] = $product->for_sale;
                $data['artworks'][$id]['category'] = $product->category;
            }

        return $data;
    }

}
