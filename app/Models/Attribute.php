<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    use HasFactory;

    protected $fillable = ['name','name_ar','slug','category_id'];

    public function category(){
        return $this->hasOne('App\Models\Category','id');
    }

    public function values(){
        return $this->hasMany('App\Models\AttributeValue');
    }

    public function getValuesAttribute(){
        return $this->values()->get();
    }
}
