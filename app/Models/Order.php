<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = ['user_id','total','shipping_fee','status','delivery_address','order_id','notes'];

    public function items(){
        return $this->hasMany('App\Models\OrderItem','order_id','id');
    }

    public function getItemsAttribute(){
        return $this->items()->get();
    }

    public function user(){
        return $this->hasOne('App\Models\User','id','user_id');
    }

    public function getUserAttribute(){
        return $this->user()->first();
    }
}
