<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\ArtistInteraction;
use App\Models\ContactEntry;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class FormController extends Controller
{
    public function __construct(Subscriber $model)
    {
        $this->model = $model;
    }

    public function subscribe(Request $request){
        $input = $request->input('email');

        $success = Subscriber::create(['email'=>$input]);

        $message = 'An error has occurred. Please try again later.';

        if($success){
            $message = 'Thank you for subscribing to our newsletter.';
        }

        return $message;
    }

    public function sendArtistEmail(Request $request){
        $input = $request->input();

        if($this->sendEmail($input))
            return 'You message has been sent to the artist.';

        return 'There was an error. Please try again later.';
    }

    public function sendInterestEmail(Request $request){
        $input = $request->input();

        if($this->sendEmail($input))
            return 'Thank you for your interest. We will get back to you the soonest.';

        return 'There was an error. Please try again later.';
    }

    public function sendEmail($input){

        $emailData = [
            'name' => $input['name'],
            'contact' => $input['contact'],
            'message' => $input['message'],
        ];

        $subject = 'Murabbaa Website - Artwork Inquiry';
        $artist = Artist::find($input['artist_id']);

        ArtistInteraction::create([
            'name' => $input['name'],
            'contact' => $input['contact'],
            'message' => $input['message'],
            'artist_id' => $input['artist_id'],
        ]);

        try {
            Mail::send('mail.send-artist', ['data' => $emailData], function ($message) use ($input, $artist,$subject) {
                $message->from('info@ajmantourism.ae', $input['name'])->to('info@ajmantourism.ae', 'Ajman Tourism')->subject($subject);
            });
        }
        catch (\Exception $e) {
            return false;
        }

        return true;
    }

    public function contact(Request $request){
        $input = $request->except('_token');

        $entry = ContactEntry::create(['ip'=>$request->ip(),'source'=>'contact-us']);

        if($entry){

            $data = [];

            foreach ($input as $key=>$item){
                $entry->items()->create(['key'=>$key,'value'=>$item]);
                $data[$key] = $item;
            }

            try {
                \Illuminate\Support\Facades\Mail::send('emails.admin.contact', ['data' => $data], function ($message) {
//                    $message->from('info@a--re.com', 'ARE Studio')->to('gene@thisishatch.com', 'Web Orders')->subject('ARE Website Contact Form Query');
                $message->from('info@a--re.com', 'ARE Studio')->to('info@a--re.com', 'Web Orders')->subject('A new order has been received.');
                });
            }
            catch (\Exception $e) {
                return $e->getMessage();
            }

            Session::flash('message','Thank you for your contacting us. We will get back to you soon.');

            return redirect()->back();
        }


        Session::flash('error','An error has occurred. Please try again later.');

        return redirect()->back();
    }
}
