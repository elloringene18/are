<?php

namespace App\Http\Controllers;

use App\Models\Alumni;
use App\Models\Article;
use App\Models\Country;
use App\Models\Education;
use App\Models\User;
use Geocoder\Laravel\Facades\Geocoder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Location;

class DirectoryController extends Controller
{
    public function alumnis(){
        $perPage = 15;
        $totalData = 0;

        $firstName = isset($_GET['first_name']) ? $_GET['first_name'] : null;
        $lastName = isset($_GET['last_name']) ? $_GET['last_name'] : null;
        $graduate_year = isset($_GET['graduate_year']) ? $_GET['graduate_year'] : null;
        $campus = isset($_GET['campus']) ? $_GET['campus'] : null;
        $country = isset($_GET['country']) ? $_GET['country'] : null;
        $page = isset($_GET['page']) ? $_GET['page'] : 1;

        $data = User::query();

//        $data->where('is_admin',0);
        $data->where('is_active',1);

        if($firstName)
            $data->where('firstName', $firstName);

        if($lastName)
            $data->where('lastName',  $lastName);

        if($graduate_year)
            $data->whereHas('education',function($query) use($graduate_year){
                return $query->where('classYear', $graduate_year);
            });

        if($campus)
            $data->whereHas('education',function($query) use($campus){
                return $query->where('campus', $campus);
            });

        if($country)
            $data->where('country', $country);

        $totalData = $data->count();
        $data = $data->with('education','career','portfolio')->limit($perPage)->skip($page == 1 ? 0 : ($page-1) * $perPage)->get();

        $mapData = User::select('country', DB::raw('count(*) as total'))->whereNotNull('country')->groupBy('country')->get()->toArray();

        foreach($mapData as $index=>$item){
            $country = Country::where('name', 'LIKE', '%' . $item['country'] . '%')->first();
            $mapData[$index]['lat'] = $country->lat;
            $mapData[$index]['long'] = $country->long;
        }

//        $campuses = Education::whereNotNull('campus')->groupBy('campus')->pluck('campus')->toArray();
        $campuses = [
            'Al Mawakeb School-Al Garhoud',
            'International School of Arts and Sciences',
            'Al Mawakeb School-Al Barsha',
            'Al Mawakeb-Al Khawaneej',
            'Al Mawakeb'
        ];

        $cities = Education::whereNotNull('campus')->groupBy('campus')->pluck('campus')->toArray();

        $countries = Country::orderBy('name','ASC')->pluck('name')->toArray();

        return view('directory', compact('mapData','data','campuses','countries','totalData','perPage','page'));
    }

    public function home(){

        $data = User::with('education','career','portfolio')->limit(1)->get();

//        $mapData = User::select('country', DB::raw('count(*) as total'))->whereNotNull('country')->groupBy('country')->get()->toArray();

        $campuses = Education::whereNotNull('campus')->groupBy('campus')->pluck('campus')->toArray();
        $countries = Country::orderBy('name','ASC')->pluck('name')->toArray();

        $news = Article::orderBy('date','DESC')->limit(6)->get();

        return view('index', compact('data','campuses','countries','news'));
    }

    public function getLocationsForMap(){

        $mapData = Location::select('location','lat','lng','total')->where('total','>',0)->get()->toArray();

        foreach($mapData as $index=>$location){
            $cut = explode(',', $location['location']);

            if(count($cut)==1){
                $mapData[$index]['city'] = null;
                $mapData[$index]['country'] = $cut[0];
            }
            if(count($cut)==2){
                $mapData[$index]['city'] = $cut[0];
                $mapData[$index]['country'] = substr($cut[1], 1);
            }
            if(count($cut)==3){
                $mapData[$index]['city'] = $cut[0];
                $mapData[$index]['country'] = substr($cut[2], 1);
            }
            if(count($cut)==4){
                $mapData[$index]['city'] = $cut[0];
                $mapData[$index]['country'] = substr($cut[3], 1);
            }
        }

        return response()->json($mapData);
    }

    public function getCitiesForCountry($id){
        $country = Country::find($id);

        return response()->json($country->cities()->get());
    }

    public function getCitiesForCountryByName($name){
        $country = Country::where('name',$name)->first();

        return response()->json($country->cities()->get());
    }

    public function alumni($id){
        $data = User::with('education','career','portfolio','skills','industries')->where('id',$id)->first();
        return view('profile', compact('data'));
    }
}
