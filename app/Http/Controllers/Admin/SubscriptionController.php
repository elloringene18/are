<?php

namespace App\Http\Controllers\Admin;

use App\Exports\SubscriberExports;
use App\Http\Controllers\Controller;

use App\Models\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class SubscriptionController extends Controller
{

    public function __construct(Subscriber $model)
    {
        $this->model = $model;
    }

    public function index(){
        $data = Subscriber::orderBy('id','DESC')->paginate(100);

        return view('admin.subscribers.index',compact('data'));
    }

    public function export(){
        return Excel::download(new SubscriberExports(), 'Mawaheb-Subscribers.xlsx');
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }


}
