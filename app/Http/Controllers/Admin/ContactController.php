<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ContactExports;
use App\Exports\SubscriberExports;
use App\Http\Controllers\Controller;

use App\Models\ContactEntry;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class ContactController extends Controller
{

    public function __construct(ContactEntry $model)
    {
        $this->model = $model;
    }

    public function index(){
        $results = ContactEntry::with('items')->orderBy('id','DESC')->paginate(100);

        $data = [];

        foreach ($results as $id=>$result){
            foreach ($result->items as $valId=>$item)
                $data[$id][$item->key] = $item->value;

            $data[$id]['id'] = $result->id;
            $data[$id]['date'] = $result->created_at;
        }

        return view('admin.inquiries.index',compact('data','results'));
    }

    public function export(){
        return Excel::download(new ContactExports(), 'Mawaheb-Inquiries.xlsx');
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

    public function view($id){
        $results = $this->model->with('items')->find($id);

        $data = [];

        $data['id'] = $results->id;
        $data['date'] = $results->created_at;

        foreach ($results->items as $item)
            $data[$item->key] = $item->value;

        return view('admin.inquiries.view',compact('data'));
    }


}
