<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Administrator;
use App\Traits\CanCreateSlug;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{
    use CanCreateSlug;

    public function __construct(Administrator $model)
    {
        $this->model = $model;
    }

    public function index(){
        $data = Administrator::paginate(12);

        return view('admin.administrators.index',compact('data'));
    }

    public function create(){
        return view('admin.administrators.create');
    }

    public function store(Request $request){
        $input = $request->except('_token');
        $photo = $request->file('photo');

        if($photo){
            $destinationPath = 'public/uploads/administrators';
            $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

            Image::make($photo->getRealPath())->fit(360, 360)->save($destinationPath.'/'.$newFileName);
            $input['photo'] = 'uploads/administrators/'. $newFileName;
        }

        Administrator::create($input);

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }

    public function edit($id){
        $item = $this->model->find($id);
        return view('admin.administrators.edit',compact('item'));
    }



    public function update(Request $request){

        $input = $request->except('_token','delete_photo','id');
        $delete_photo = $request->input('delete_photo');
        $photo = $request->file('photo');
        $target = $this->model->find($request->input('id'));

        if($target){

            if($photo){
                $destinationPath = 'public/uploads/administrators';
                $newFileName = Str::random(32).'.'.$photo->getClientOriginalExtension();

                Image::make($photo->getRealPath())->fit(360, 360)->save($destinationPath.'/'.$newFileName);
                $input['photo'] = 'uploads/administrators/'. $newFileName;
            }

            if($delete_photo)
                $input['photo'] = null;

            $target->update($input);

            Session::flash('success','Item updated successfully.');
            return redirect()->back();
        }

        return redirect()->back();
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

}
