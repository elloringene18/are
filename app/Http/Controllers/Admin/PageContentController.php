<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PageContent;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class PageContentController extends Controller
{
    public function __construct(PageContent $model)
    {
        $this->model = $model;
    }

    public function edit($page){
        $data = PageContent::where('page',$page)->get();
        return view('admin.page-contents.index',compact('data'));
    }

    public function update(Request $request){

        $input = $request->except('_token');

        foreach($input['section'] as $id=>$item){
            $target = $this->model->find($id);

            if($target){
                $target->update($item);
            }
        }

        Session::flash('success','Item updated successfully.');
        return redirect()->back();
    }
}
