<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductVariant;
use App\Models\Seller;
use App\Models\Upload;
use App\Services\Uploaders\FileUploader;
use App\Services\Uploaders\ProductImagesUploader;
use App\Traits\CanCreateSlug;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    use CanCreateSlug;

    public function __construct(Product $model, ProductImagesUploader $uploader)
    {
        $this->model = $model;
        $this->uploader = $uploader;
    }

    public function index(){
        $data = Product::with('category')->orderBy('id','DESC')->paginate(100);

        $brands = Seller::get();
        $categories = Category::get();

        return view('admin.products.index',compact('data','brands','categories'));
    }

    public function showNoPhotos(){

        $products = Product::get();

        echo '<table><tr><td>Item</td><td>Brand</td></tr>';
        foreach($products as $product){
            if(!$product->photo)
                echo '<tr><td>'.$product->title . '</td><td>' . $product->seller->name .'</td></tr>';
        }

        dd('end');
    }

    public function filter(Request $request){
        $input = $request->input();
        $query = Product::query();

        if(isset($input['brand'])){
            if($input['brand'] != 'all')
                $query->whereHas('seller',function($query) use($input){
                    return $query->where('slug',$input['brand']);
                });
        }

        if(isset($input['category'])) {
            if ($input['category'] != 'all')
                $query->whereHas('category', function ($query) use ($input) {
                    return $query->where('slug', $input['category']);
                });
        }

        if(isset($input['sort'])) {
            if ($input['sort'] == 'name-desc')
                $query->orderBy('title','DESC');
            elseif ($input['sort'] == 'name-asc')
                $query->orderBy('title','ASC');
            else
                $query->orderBy('id','DESC');
        }

        $data = $query->with('category')->orderBy('id', 'DESC')->paginate(100);

        $brands = Seller::get();
        $categories = Category::get();

        return view('admin.products.index',compact('data','brands','categories'));
    }

    public function create(){
        $categories = Category::get();
        $sellers = Seller::get();
        return view('admin.products.create',compact('categories','sellers'));
    }

    public function show(){
        $pages = $this->model->get();
        return view('admin.products.show',compact('pages'));
    }

    public function store(Request $request){
        $image = $request->file('image');
        $input = $request->except('_token','image','attributes','seller_id','variants');
        $attributes = $request->input('attributes');
        $variants = $request->input('variants');

        if($image){
            $img = Image::make($image);
            $img->resize(260, 260);

            $destinationPath = 'public/uploads/products';
            $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

            Image::make($image->getRealPath())->fit(360, 360)->save($destinationPath.'/'.$newFileName);
            $input['photo'] = 'uploads/products/'. $newFileName;

            $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();
            Image::make($image->getRealPath())->resize(680,680)->save($destinationPath.'/'.$newFileName);
            $input['photo_full'] = 'uploads/products/'. $newFileName;
        }

        $input['slug'] = $this->generateSlug($input['title']);

        $product = Product::create($input);

        $product->attributes()->sync($attributes);
        $product->seller()->sync($request->input('seller_id'));

        $photos = $request->file('photos');

        if($product && $photos) {
            foreach ($photos as $file) {
                $photo = ($file != null ? $this->uploader->upload($file) : false);
                if ($photo)
                    $product->uploads()->createMany($photo);
            }
        }

        if($variants){
            foreach ($variants as $variant){
                if($variant['name']){
                    $v['key'] = $variant['name'];
                    foreach ($variant['values'] as $var){
                        $v['price'] = $var['price'];
                        $v['value'] = $var['value'];
                        $product->variants()->create($v);
                    }
                }
            }
        }

        Session::flash('success','Item successfully added.');
        return redirect()->back();
    }

    public function edit($id){

        $categories = Category::get();
        $item = $this->model->with('category.attributes.values')->find($id);

        $currentAttributes = [];

        foreach($item->attributes as $attribute){
            $currentAttributes[] = $attribute->id;
        }

        $currentAttributes = json_encode($currentAttributes);
        $sellers = Seller::get();

        $productVariants = ProductVariant::where('product_id',$item->id)->get();
        $variants = [];

        if($productVariants){
            foreach($productVariants as $variant){
                $variants[$variant->key]['key'] = $variant->key;
                $variants[$variant->key]['values'][$variant->id]['id'] = $variant->id;
                $variants[$variant->key]['values'][$variant->id]['price'] = $variant->price;
                $variants[$variant->key]['values'][$variant->id]['value'] = $variant->value;
            }
        }

        return view('admin.products.edit',compact('categories','item','currentAttributes','sellers','variants'));
    }



    public function update(Request $request){
        $image = $request->file('photo');
        $input = $request->except('_token','image','attributes','id');
        $attributes = $request->input('attributes');
        $key = $request->input('key');
        $variantValues = $request->input('values');
        $variants = $request->input('newvariants');

        $target = $this->model->find($request->input('id'));

        if($target){
            if($image){
                $img = Image::make($image);
                $img->resize(260, 260);

                $destinationPath = 'public/uploads/products';
                $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();

                Image::make($image->getRealPath())->fit(360, 360)->save($destinationPath.'/'.$newFileName);
                $input['photo'] = 'uploads/products/'. $newFileName;

                $newFileName = Str::random(32).'.'.$image->getClientOriginalExtension();
                Image::make($image->getRealPath())->fit(680, 680)->save($destinationPath.'/'.$newFileName);
                $input['photo_full'] = 'uploads/products/'. $newFileName;

            }

            if($input['title']!=$target->title)
                $target->slug = $this->generateSlug($input['title']);

            $target->update($input);
            $target->attributes()->sync($attributes);
        }

        $photos = $request->file('photos');
        $target->seller()->sync($request->input('seller_id'));

        if($target && $photos) {
            foreach ($photos as $file) {
                $photo = ($file != null ? $this->uploader->upload($file) : false);
                if ($photo)
                    $target->uploads()->createMany($photo);
            }
        }

        if($variantValues){
            foreach ($variantValues as $id=>$variant){
                $targetVariant = ProductVariant::find($id);
                $v['key'] = $key;
                $v['price'] = $variant['price'];
                $v['value'] = $variant['value'];
                $targetVariant->update($v);
            }
        }

        if($variants){
            foreach ($variants as $variant){
                if($key && $variant['price'] && $variant['value']){
                    $v['key'] = $key;
                    $v['price'] = $variant['price'];
                    $v['value'] = $variant['value'];
                    $target->variants()->create($v);
                }
            }
        }

        Session::flash('success','Item updated successfully.');
        return redirect()->back();
    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

    public function deleteValue($id){
        $page = ProductVariant::find($id);

        if($page)
            $page->delete();

        Session::flash('success','Item deleted successfully.');

        return redirect()->back();
    }

    public function deletePhoto($id){
        $page = Upload::find($id);

        if($page)
            $page->delete();

        Session::flash('success','Item deleted successfully.');

        return redirect()->back();
    }

    /**
     * Resizes a image using the InterventionImage package.
     *
     * @param object $file
     * @param string $fileNameToStore
     * @author Niklas Fandrich
     * @return bool
     */
    public function resizeImage($file) {
        // Resize image
        $resize = Image::make($file)->resize(600, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        return $resize;
    }

}
