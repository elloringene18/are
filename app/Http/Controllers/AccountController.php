<?php

namespace App\Http\Controllers;

use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use League\CommonMark\Extension\Attributes\Util\AttributesHelper;

class AccountController extends Controller
{
    public function index()
    {
        $data = Auth::user()->orders;
        return view('account',compact('data'));
    }

    public function order($id)
    {
        $order = Order::with('items.product')->find($id);
        return view('order',compact('order'));
    }

    public function add($product_id)
    {
        Auth::user()->favourites()->sync([$product_id],false);
        return redirect()->back();
    }

    public function moveToCart($product_id)
    {
        Auth::user()->cart()->create(['product_id'=>$product_id]);
        Auth::user()->favourites()->detach([$product_id],false);
        return redirect()->back();
    }

    public function delete($product_id)
    {
        Auth::user()->favourites()->detach($product_id);
        return redirect()->back();
    }
}
