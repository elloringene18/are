<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\UserCartItem;
use App\Traits\Crypto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    use Crypto;

    public function index()
    {
        $data = Auth::user()->cart()->get();
        return view('cart',compact('data'));
    }

    public function quickAdd($product_id)
    {
        Auth::user()->cart()->create(['product_id'=>$product_id]);
        return response()->json(['success'=>1,'message'=>'Item added to cart. Click <a href="'.url('cart').'">here</a> to see cart.']);
    }

    public function add(Request $request)
    {
        Auth::user()->cart()->create($request->except('_token'));
        return response()->json(['success'=>1,'message'=>'Item added to cart. Click <a href="'.url('cart').'">here</a> to see cart.']);
    }

    public function delete($id)
    {
        $target = Auth::user()->cart()->find($id);

        $target->delete();

        Session::flash('success','Item has removed your cart.');

        return redirect()->back();
    }

    public function reduce($cart_item_id)
    {
        $item = UserCartItem::find($cart_item_id);

        if($item->user_id != Auth::user()->id)
            return 'You are not allowed to make this change!';

        if($item->qty>1){
            $item->qty -= 1;
            $item->save();

            Session::flash('success','Item quantity updated.');
        }
        else {
            $item->delete();
            Session::flash('success','Item has been removed.');
        }

        return redirect()->back();
    }

    public function increase($cart_item_id)
    {
        $item = UserCartItem::find($cart_item_id);

        if($item->user_id != Auth::user()->id)
            return 'You are not allowed to make this change!';

        $item->qty += 1;
        $item->save();
        Session::flash('success','Item quantity updated.');

        return redirect()->back();
    }

    public function checkout()
    {
        $data = Auth::user()->cart()->get();
        return view('checkout',compact('data'));
    }

    public function order(Request $request)
    {
        $profile = $request->input('profile');
        $address = $request->input('address');
        $user = Auth::user();
        $cart = $user->cart;

        $subtotal = 0;
        $total = 0;
        $shipping = 0;

        foreach($cart as $item){
            $subtotal += $item->product_variant_id ? $item->variant->price * $item->qty : $item->product->price * $item->qty;
        }

        $total = $subtotal + $shipping;

        if($user->profile)
            $user->profile->update($profile);
        else
            $user->profile()->create($profile);

        if($user->address)
            $user->address->update($address);
        else
            $user->address()->create($address);

        $fullAddress = null;

        foreach($address as $add){

            if($add)
                $fullAddress .= $add;

            $fullAddress .= ', ';
        }

        $order = $user->orders()->create([
            'total' => $total,
            'shipping' => $shipping,
            'status' => 'unpaid',
            'delivery_address' => $fullAddress
        ]);

        foreach ($cart  as $item)
            $order->items()->create([
                'product_id' => $item->product_id,
                'product_variant_id' => $item->product_variant_id,
                'qty' => $item->qty,
                'total' => $item->product_variant_id ? $item->variant->price * $item->qty : $item->product->price * $item->qty,
            ]);

//        $user->cart()->delete();


        // PAYMENT GATEWAY

        $merchant_data='48592';
        $working_key='3C929B2E8E8FD75709538A1263339D81';//Shared by CCAVENUES
        $access_code='AVFB03IH87CL53BFLC';//Shared by CCAVENUES

        $ccaveData = [];

        $ccaveData['_token'] = $request->input('_token');
        $ccaveData['merchant_id'] = $merchant_data;
        $ccaveData['order_id'] = strval($order->id);
        $ccaveData['amount'] = strval($total);
        $ccaveData['currency'] = 'AED';
        $ccaveData['redirect_url'] = url('ccav-response-handler');
        $ccaveData['cancel_url'] = url('ccav-response-handler');
        $ccaveData['language'] = 'EN';

        $ccaveData['delivery_name'] = $profile['name'];
        $ccaveData['delivery_address'] = $address['apartment'].', '.$address['building'].', '.$address['street'].', '.$address['city'].', '.$address['country'];
        $ccaveData['delivery_city'] = $address['city'];
        $ccaveData['delivery_country'] = $address['country'];
        $ccaveData['delivery_tel'] = $profile['mobile'];

        $ccaveData['billing_name'] = $profile['name'];
        $ccaveData['billing_address'] = $address['apartment'].', '.$address['building'].', '.$address['street'].', '.$address['city'].', '.$address['country'];
        $ccaveData['billing_city'] = $address['city'];
        $ccaveData['billing_country'] = $address['country'];
        $ccaveData['billing_tel'] = $profile['mobile'];
        $ccaveData['billing_email'] = $profile['email'];

        foreach ($ccaveData as $key => $value){
            $merchant_data.=$key.'='.$value.'&';
        }

        $encrypted_data= $this->encrypt($merchant_data,$working_key); // Method for encrypting the data.

        return view('test.redirect', compact('encrypted_data','access_code'));

    }

    public function payment(Request $request)
    {
        dd($request->input());
    }

    public function ccavRequestHandler(Request $request)
    {
        error_reporting(0);

        $merchant_data='48592';
        $working_key='3C929B2E8E8FD75709538A1263339D81';//Shared by CCAVENUES
        $access_code='AVFB03IH87CL53BFLC';//Shared by CCAVENUES

        foreach ($_POST as $key => $value){
            $merchant_data.=$key.'='.$value.'&';
        }

        $encrypted_data= $this->encrypt($merchant_data,$working_key); // Method for encrypting the data.

//        Session::flash('success','You order has been placed.');
//
//        return redirect('account');
        return view('test.redirect', compact('encrypted_data','access_code'));
    }

    public function ccavResponseHandler(Request $request)
    {
        $post = $request->except('_token');
        error_reporting(0);

        $workingKey='3C929B2E8E8FD75709538A1263339D81';		//Working Key should be provided here.
        $encResponse=$post["encResp"];			//This is the response sent by the CCAvenue Server
        $rcvdString=$this->decrypt($encResponse,$workingKey);		//Crypto Decryption used as per the specified working key.
        $order_status="";
        $decryptValues=explode('&', $rcvdString);
        $dataSize=sizeof($decryptValues);

//        echo "<center>";

        for($i = 0; $i < $dataSize; $i++)
        {
            $information=explode('=',$decryptValues[$i]);
            if($i==0)
                $order_id=$information[1];
            if($i==3)
                $order_status=$information[1];
        }


        if($order_status==="Success")
        {
            $order = Order::find($order_id);

            if($order && $order->status != 'paid'){
                $order->update(['status'=>'paid']);

                $this->sendEmailClient($order->id);
            }

            Session::flash('success','You order has been placed.');
            return view('order',compact('order'));

        }
        else if($order_status==="Aborted")
        {
            $order = Order::find($order_id);

            if($order)
                $order->delete();

            echo 'The transaction has been aborted. Please click <a href="'.url('cart/checkout').'">here</a> to return to the checkout page.';
        }
        else if($order_status==="Failure")
        {
            $order = Order::find($order_id);

            if($order)
                $order->delete();

            echo 'The transaction has been declined. Please click <a href="'.url('cart/checkout').'">here</a> to return to the checkout page.';
        }
        else
        {
            echo "<br>Security Error. Illegal access detected";

        }


//        echo "<br><br>";
//
//        echo "<table cellspacing=4 cellpadding=4>";
//        for($i = 0; $i < $dataSize; $i++)
//        {
//            $information=explode('=',$decryptValues[$i]);
//            echo '<tr><td>'.$information[0].'</td><td>'.$information[1].'</td></tr>';
//        }
//https://secure.ccavenue.ae/receive/1/servlet/BankRespReceive?vpc_3DSECI=02&vpc_3DSXID=RzjsCYpdRmeXItnd707brEAJlxY%3D&vpc_3DSenrolled=Y&vpc_3DSstatus=Y&vpc_AVSRequestCode=Z&vpc_AVSResultCode=Unsupported&vpc_AcqAVSRespCode=Unsupported&vpc_AcqCSCRespCode=Unsupported&vpc_AcqResponseCode=00&vpc_Amount=910000&vpc_AuthorizeId=540212&vpc_BatchNo=20210706&vpc_CSCResultCode=Unsupported&vpc_Card=MC&vpc_CardNum=xxxxxxxxxxxx0014&vpc_Command=pay&vpc_Locale=en&vpc_MerchTxnRef=110019347162&vpc_Merchant=TEST800206&vpc_Message=Approved&vpc_OrderInfo=110019347162&vpc_ReceiptNo=118722540212&vpc_SecureHash=FA84AB608FE5D7B3B78FBC9FC62D9B9A2E8D75BFE6E65C5A2B86BE3A6055A53D&vpc_SecureHashType=SHA256&vpc_TransactionNo=30000062335&vpc_TxnResponseCode=0&vpc_VerSecurityLevel=05&vpc_VerStatus=Y&vpc_VerToken=jHyn%2B7YFi1EUAREAAAAvNUe6Hv8%3D&vpc_VerType=3DS&vpc_Version=1
//        echo "</table><br>";
//        echo "</center>";
    }

    public function getCount()
    {
        return Auth::user()->cart()->count();
    }

    public function sendEmail($order_id){

        $order = Order::with('user.profile','items.product','items.variant')->where('id',$order_id)->first();
        $data['order'] = $order;
        $data['user'] = $order->user->name;
        $data['mobile'] = $order->user->profile->mobile;
        $data['email'] = $order->user->profile->email;
        $data['address'] = $order->delivery_address;

        foreach($order->items as $id=>$item){
            $data['products'][$id] = $item->product;
            if($item->variant)
                $data['products'][$id]['variant'] = $item->variant;
        }

        try {
            Mail::send('emails.admin.order', ['data' => $data], function ($message) {
                $message->from('info@a--re.com', 'ARE Studio')->to('gene@thisishatch.com', 'Web Orders')->subject('A new order has been received.');
//                $message->from('info@a--re.com', 'ARE Studio')->to('info@a--re.com', 'Web Orders')->subject('A new order has been received.');
            });
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }

        return true;
    }

    public function sendEmailClient($order_id){

        $order = Order::with('user.profile','items.product','items.variant')->where('id',$order_id)->first();
        $data['order'] = $order;
        $data['user'] = $order->user->name;
        $data['mobile'] = $order->user->profile->mobile;
        $data['email'] = $order->user->profile->email;
        $data['address'] = $order->delivery_address;

        foreach($order->items as $id=>$item){
            $data['products'][$id] = $item->product;
            if($item->variant)
                $data['products'][$id]['variant'] = $item->variant;
        }

        try {
            Mail::send('emails.customers.order', ['data' => $data], function ($message) use($data) {
                $message->from('info@a--re.com', 'ARE Studio')->to($data['email'], $data['user'])->subject('Thank you for your order.');
            });
        }
        catch (\Exception $e) {
            return $e->getMessage();
        }

        return true;
    }
}
