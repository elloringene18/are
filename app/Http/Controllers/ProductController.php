<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\ProductVariant;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function show($slug){

        $product = Product::with('category.attributes.values','variants')->where('slug','like','%'.$slug.'%')->first();
        if(empty($product)){
            $product = Product::where('slug',$slug)->first();
        }
        $productVariants = ProductVariant::where('product_id',$product->id)->get();
        $variants = [];

        if($productVariants){
            foreach($productVariants as $variant){
                $variants[$variant->key][$variant->id]['id'] = $variant->id;
                $variants[$variant->key][$variant->id]['price'] = $variant->price;
                $variants[$variant->key][$variant->id]['value'] = $variant->value;
            }
        }

        return view('product',compact('product','variants'));
    }

    private function langFilter($data,$lang){
        $results = [];

        if($lang=='en'){
            $results['title'] = $data->title;
            $results['description'] = $data->description;
        }
        else {
            $results['title'] = $data->title_ar;
            $results['description'] = $data->description_ar;
        }

        $results['id'] = $data->id;
        $results['slug'] = $data->slug;
        $results['for_sale'] = $data->for_sale;
        $results['photo'] = asset('public/'.$data->photo);
        $results['photo_full'] = asset('public/'.$data->photo_full);
        $results['price'] = $data->price;
        $results['is_faved'] = $data->is_faved;
        $results['category'] = $data->category;
//            $results['artist'] = $data->artist;

        $results['attributes'] = [];

        foreach ($data->attributes as $id=>$attribute){
            $results['attributes'][$attribute->attribute->name]['value'][] = $lang=='en' ? $attribute->value : $attribute->value_ar;
        }

        return $results;
    }
}
