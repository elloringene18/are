<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Artist;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function getFromArtist($id){

        $artist = Artist::find($id);
        $data = [];

        if($artist) {
            if($artist->products){
                foreach ($artist->products as $item){
                    $results = [];

                    $results['id'] = $item->id;
                    $results['title'] = $item->title;
                    $results['photo'] = $item->thumbnailUrl;

                    $data[] = $results;
                }
            }
        }

        return $data;
    }

}
