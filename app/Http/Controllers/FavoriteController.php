<?php

namespace App\Http\Controllers;

use App\Models\UserFavourite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use League\CommonMark\Extension\Attributes\Util\AttributesHelper;

class FavoriteController extends Controller
{
    public function index()
    {
        $products = Auth::user()->favourites;
        return view('favourites',compact('products'));
    }

    public function add($product_id)
    {
        Auth::user()->favourites()->sync([$product_id],false);

        Session::flash('success','Item has been added to favourites.');
        return redirect()->back();
    }

    public function toggle($product_id)
    {
        $target = UserFavourite::where('product_id',$product_id)->where('user_id',Auth::user()->id)->first();

        if($target){
            $target->delete();
            return 0;
        }
        else
            Auth::user()->favourites()->sync([$product_id],false);

        return 1;
    }

    public function getFavourites()
    {
        return Auth::user()->favourites()->count();
    }

    public function moveToCart($product_id)
    {
        Auth::user()->cart()->create(['product_id'=>$product_id]);
        Auth::user()->favourites()->detach([$product_id],false);

        Session::flash('success','Item has been moved to cart.');

        return redirect()->back();
    }

    public function delete($product_id)
    {
        Auth::user()->favourites()->detach($product_id);

        Session::flash('success','Item has been removed from favourites.');
        return redirect()->back();
    }
}
