

$('.overlay .wrap').on('click',function (e) {
    e.preventDefault();
    $(this).closest('.overlay').removeClass('active');
});

$('.poplink').on('click',function (e) {
    e.preventDefault();
    $('.overlay').removeClass('active');
    $('#'+$(this).attr('data-target')).toggleClass('active');
});

$('.close-panel').on('click',function (e) {
    e.preventDefault();
    $(this).closest('.overlay').removeClass('active');
});
