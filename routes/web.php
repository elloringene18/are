<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $products = \App\Models\Product::orderBy('id', 'DESC')->paginate(6);

    return view('index', compact('products'));
});

Route::get('/landing', function () {
    return view('landing');
});

Route::get('/emails/customer/order', function () {

    $order = \App\Models\Order::with('user.profile', 'items.product', 'items.variant')->where('id', 6)->first();
    $data['order'] = $order;
    $data['user'] = $order->user->name;
    $data['mobile'] = $order->user->profile->mobile;
    $data['email'] = $order->user->profile->email;
    $data['address'] = $order->delivery_address;

    foreach ($order->items as $id => $item) {
        $data['products'][$id] = $item->product;
        if ($item->variant)
            $data['products'][$id]['variant'] = $item->variant;
    }

    try {
        \Illuminate\Support\Facades\Mail::send('emails.admin.order', ['data' => $data], function ($message) {
            $message->from('info@a--re.com', 'ARE Studio')->to('gene@thisishatch.com', 'Web Orders')->subject('A new order has been received.');
            //                $message->from('info@a--re.com', 'ARE Studio')->to('info@a--re.com', 'Web Orders')->subject('A new order has been received.');
        });
    } catch (\Exception $e) {
        return $e->getMessage();
    }

    //    $order = \App\Models\Order::with('user.profile','items.product','items.variant')->orderBy('id','DESC')->first();
    //    $data['order'] = $order;
    //    $data['user'] = $order->user->name;
    //    $data['mobile'] = $order->user->profile->mobile;
    //    $data['email'] = $order->user->profile->email;
    //    $data['address'] = $order->delivery_address;
    //
    //    foreach($order->items as $id=>$item){
    //        $data['products'][$id] = $item->product;
    //        if($item->variant)
    //            $data['products'][$id]['variant'] = $item->variant;
    //    }
    //
    return view('emails.customers.order', compact('data'));
});

Route::get('/emails/admin/order', function () {
    $order = \App\Models\Order::with('user.profile', 'items.product', 'items.variant')->orderBy('id', 'DESC')->first();
    $data['order'] = $order;
    $data['user'] = $order->user->name;
    $data['mobile'] = $order->user->profile->mobile;
    $data['email'] = $order->user->profile->email;
    $data['address'] = $order->delivery_address;

    foreach ($order->items as $id => $item) {
        $data['products'][$id] = $item->product;
        if ($item->variant)
            $data['products'][$id]['variant'] = $item->variant;
    }

    return view('emails.admin.order', compact('data'));
});

Route::get('/get-cities-for-country/{id}', 'DirectoryController@getCitiesForCountry');
Route::get('/get-cities-for-country-name/{name}', 'DirectoryController@getCitiesForCountryByName');

Route::get('/shop', 'ShopController@index');
Route::get('/collection-single', 'ShopController@collection');
Route::get('/collection-single/{cat}', 'ShopController@collection_id');
Route::get('/shop/{category}', 'ShopController@index');
Route::get('/brand/{seller}', 'ShopController@seller');

Route::get('/product/{slug}', 'ProductController@show');
Route::post('/search/', 'SearchController@search');

Route::post('/customer-login', 'Auth\LoginController@authenticate');
Route::post('/register', 'Auth\RegisterController@register');

Route::post('/subscribe', 'FormController@subscribe');
Route::post('/contact', 'FormController@contact');

Route::get('/test-checkout', function () {
    return view('test.checkout');
});

Route::post('/ccav-request-handler', 'CartController@ccavRequestHandler');
Route::post('/ccav-response-handler', 'CartController@ccavResponseHandler');

Route::auth();

Route::get('/login', function () {
    return view('auth.login');
})->name('login');

Route::group(['middleware' => 'auth'], function () {
    Route::get('/favourites', 'FavoriteController@index');
    Route::get('/favourites/toggle/{product_id}', 'FavoriteController@toggle');
    Route::get('/favourites/add/{product_id}', 'FavoriteController@add');
    Route::get('/favourites/delete/{product_id}', 'FavoriteController@delete');
    Route::get('/favourites/move-to-cart/{product_id}', 'FavoriteController@moveToCart');
    Route::get('/favourites/get-favourites', 'FavoriteController@getFavourites');

    Route::get('/cart', 'CartController@index');
    Route::get('/cart/add/{product_id}', 'CartController@quickAdd');
    Route::post('/cart/add', 'CartController@add');
    Route::get('/cart/reduce/{cart_item_id}', 'CartController@reduce');
    Route::get('/cart/increase/{cart_item_id}', 'CartController@increase');
    Route::get('/cart/delete/{product_id}', 'CartController@delete');
    Route::get('/cart/checkout/', 'CartController@checkout');
    Route::post('/cart/checkout/', 'CartController@order');
    Route::get('/cart/get-count/', 'CartController@getCount');

    Route::get('/account/', 'AccountController@index');
    Route::get('/account/order/{id}', 'AccountController@order');

    Route::get('/logout', function () {
        Auth::logout();
        return redirect('/');
    });
});

Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'is_admin']], function () {
    Route::get('/', function () {
        return view('dashboard');
    });

    Route::group(['prefix' => 'orders', 'middleware' => 'auth'], function () {
        Route::get('/', 'Admin\OrderController@index');
        Route::post('/update', 'Admin\OrderController@update');
        Route::get('/delete/{id}', 'Admin\OrderController@delete');
        Route::get('/{id}', 'Admin\OrderController@view');
        Route::get('/update-status/{order_id}/{status}', 'Admin\OrderController@updateStatus');
    });

    // Route::group(['prefix' => 'Categories', 'middleware' => 'auth'], function () {
    //     Route::get('/', 'Admin\OrderController@index');
    // });

    Route::group(['prefix' => 'products', 'middleware' => 'auth'], function () {
        Route::get('/no-photos', 'Admin\ProductController@showNoPhotos');
        Route::get('/', 'Admin\ProductController@index');
        Route::get('/filter', 'Admin\ProductController@filter');
        Route::get('/create', 'Admin\ProductController@create');
        Route::post('/store', 'Admin\ProductController@store');
        Route::post('/update', 'Admin\ProductController@update');
        Route::get('/delete/{id}', 'Admin\ProductController@delete');
        Route::get('/delete/value/{id}', 'Admin\ProductController@deleteValue');
        Route::get('/delete-photo/{id}', 'Admin\ProductController@deletePhoto');
        Route::get('/{id}', 'Admin\ProductController@edit');
    });

    Route::group(['prefix' => 'categories', 'middleware' => 'auth'], function () {
        Route::get('/', 'Admin\CategoryController@index');
        Route::post('/store', 'Admin\CategoryController@store');
        Route::get('/create', 'Admin\CategoryController@create');
        Route::post('/update', 'Admin\CategoryController@update');
        Route::get('/delete/{id}', 'Admin\CategoryController@delete');
        Route::get('/{id}', 'Admin\CategoryController@edit');
    });

    Route::group(['prefix' => 'brands', 'middleware' => 'auth'], function () {
        Route::get('/', 'Admin\SellerController@index');
        Route::post('/store', 'Admin\SellerController@store');
        Route::get('/create', 'Admin\SellerController@create');
        Route::post('/update', 'Admin\SellerController@update');
        Route::get('/{id}/delete', 'Admin\SellerController@delete');
        Route::get('/delete-collection/{id}', 'Admin\SellerController@deleteCollection');
        Route::get('/{id}', 'Admin\SellerController@edit');
    });


    Route::group(['prefix' => 'page-contents', 'middleware' => 'auth'], function () {
        Route::post('/update', 'Admin\PageContentController@update');
        Route::get('/{page}', 'Admin\PageContentController@edit');
    });

    Route::group(['prefix' => 'subscribers', 'middleware' => 'auth'], function () {
        Route::get('/', 'Admin\SubscriptionController@index');
        Route::get('/export', 'Admin\SubscriptionController@export');
        Route::get('/delete/{id}', 'Admin\SubscriptionController@delete');
    });

    Route::group(['prefix' => 'contacts', 'middleware' => 'auth'], function () {
        Route::get('/', 'Admin\ContactController@index');
        Route::get('/export', 'Admin\ContactController@export');
        Route::get('/view/{id}', 'Admin\ContactController@view');
        Route::get('/delete/{id}', 'Admin\ContactController@delete');
    });
});

Route::get('news/{slug}', function ($page) {
    return view('articles.' . $page);
});

Route::get('/{page}', function ($page) {
    return view($page);
});
